public class CaseOwnerManager {
    
    private static Set<Id> ownerIds;
    private static Map<Id,Id> userToManagaerMap;
    
    static{
        ownerIds = new Set<Id>();
        userToManagaerMap = new Map<Id,Id>();
    }
    
    public static void reassignToManager(List<Case> cases, Map<Id,Case> caseOldMap){
        
        for(Case c : cases){
            if(SObjectUtility.isNewOrChanged(c, 'Home_Visit_SLA_Missed__c', trigger.oldMap) && c.OwnerId != null){
                
                ownerIds.add(c.ownerId);
                
            }
            
        }
        
        if(!ownerIds.isEmpty()){
            List<User> owners = [SELECT Id, Manager.Id FROM User WHERE Id IN :ownerIds];   
            
            for(User user : owners){
                userToManagaerMap.put(user.Id,user.Manager.Id);
            }
            
            for(Case c : cases){
                try{
                    c.OwnerId = userToManagaerMap.get(c.OwnerId);
                    c.Status = 'Escalated';
                }
                catch(Exception ex){
                    system.debug('Exception in Case trigger Handler CaseOwnerManager'+ex);
                }
            } 
        }   
        
        
    }
    
}
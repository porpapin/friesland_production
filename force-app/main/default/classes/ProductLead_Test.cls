@isTest
public class ProductLead_Test {
    
    private static void createRecords(){
        
        List<Product2> products = new List<Product2>();
        products.add(new Product2(Name='Milk', ProductCode='m1',Country__c = 'Vietnam'));
        products.add(new Product2(Name='Chocolate Milk', ProductCode='m2',Country__c = 'Vietnam'));
        products.add(new Product2(Name='Current Milk', ProductCode='m3',Country__c = 'Vietnam'));
        insert products;
        
        List<Lead> leads = new List<Lead>();
        
        for(Integer i=1; i<=1; i++){
            leads.add( new Lead(FirstName='Test1', LastName='Test 1', MobilePhone = '0709799559', Status='New', Company='Vietnam', Product_Requested_Code__c='m1', Current_Product_Code__c='m1',Child_dob__c = system.today(),Product_Requested__c = products[0].Id,Current_Product_Used__c=products[0].Id) );
        }

        for(Integer i=11; i<=20; i++){
            leads.add( new Lead(FirstName='Test'+i, LastName='Test'+i, MobilePhone = '0709799559', Status='New', Company='Vietnam', Product_Requested_Code__c='m2', Current_Product_Code__c='m2',Child_dob__c = system.today(),Product_Requested__c = products[1].Id,Current_Product_Used__c=products[1].Id) );
        }
        // Test 3 - Leads with Requested Product but no Current Product
        for(Integer i=11; i<=20; i++){
            leads.add( new Lead(FirstName='Test'+i, LastName='Test'+i, MobilePhone = '0709799559', Status='New', Company='Vietnam', Product_Requested_Code__c='m3',Current_Product_Code__c='m3',Child_dob__c = system.today(),Product_Requested__c = products[2].Id,Current_Product_Used__c=products[2].Id) );
        }
        insert leads;
        
    }
    /*
    public static testMethod void testNewLeads(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            
            createRecords();
            
            String requestedProductId = [SELECT Id FROM Product2 WHERE ProductCode='m1'][0].id;
            String currentProductId = [SELECT Id FROM Product2 WHERE ProductCode='m1'][0].id;
            
            for(Lead lead : [SELECT Product_Requested__c, Current_Product_Used__c FROM Lead
                             WHERE LastName = 'Test 1']){
                                 System.assert(lead.Product_Requested__c == requestedProductId);
                                 System.assert(lead.Current_Product_Used__c == currentProductId);
                             }
            
            
            for(Lead lead : [SELECT Product_Requested__c, Current_Product_Used__c FROM Lead
                             WHERE LastName = 'Test 2']){
                                 System.assert(lead.Product_Requested__c == requestedProductId);
                                 System.assert(lead.Current_Product_Used__c == currentProductId);
                             }
            
            for(Lead lead : [SELECT Product_Requested__c, Current_Product_Used__c FROM Lead
                             WHERE LastName = 'Test 3']){
                                 System.assert(lead.Product_Requested__c == requestedProductId);
                                 System.assert(lead.Current_Product_Used__c == null);
                             }
            
        }
    }
	*/
    
    //update all leads to ProductCode = m1
    public static testMethod void testUpdateLeads(){
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            createRecords();
            
            Id requestedProductId = [SELECT Id FROM Product2 WHERE ProductCode='m2'][0].id;
            List<Lead> newLeadsList = new List<Lead>();
            List<Lead> leadsList = [SELECT Product_Requested_Code__c FROM Lead];
            for (Lead lead : leadsList){
                if( !lead.Product_Requested_Code__c.equals('m1') ){
                    lead.Product_Requested_Code__c = 'm1';
                    lead.Product_Requested__c = requestedProductId;
                    newLeadsList.add(lead);
                }
            }
            Test.startTest();
            if(newLeadsList.size() > 0){
                update leadsList;
            }
            Test.stopTest();
            
            leadsList = [SELECT Product_Requested__c FROM Lead];
            String productId = [SELECT Id FROM Product2 WHERE ProductCode='m1'][0].id;
            for (Lead lead : leadsList){
                System.assert(lead.Product_Requested__c == productId);
            }
        }
    }
}
/*
 * Name : LeadDuplicateFlow_Tests
 * Created By : Naresh Kumar
 * Description: Class is used to test  'LeadDuplicateCategorizationFlow' Flow
 * Created Date: [13 Nov 2018]
 * Task : T-749469
 * 
 * */
@isTest
public class LeadDuplicateFlow_Tests {
    
    @IsTest 
    public static void when_leadMatchesOnEmail_existingContact_ShouldBe_Populated(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,Date.today(),null,'Vietnam');
        //Diffrent Country 
        TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,Date.today().addDays(1),null,'Japan');
        //diffrent recordtype
        TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',childRecordTypeId,acc.id,Date.today().addDays(2),null,'Vietnam');
        //diffrent email
        TestDataForLeadFlow.createContact('test@dontMatch.com','(081) 987-6524',parentRecordTypeId,acc.id,Date.today().addDays(3),null,'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('test@test.com','0709799559',Date.today(),null,'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        system.assertEquals(leadList[0].Existing_Contact__r.id ,matchContact.id);
    }
    
    @IsTest
    public static void when_leadMatchesOnMobilePhone_existingContact_ShouldBe_Populated(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,Date.today(),null,'Vietnam');
        //Diffrent Country 
        TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,Date.today().addDays(1),null,'Japan');
        //diffrent recordtype
        TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',childRecordTypeId,acc.id,Date.today().addDays(2),null,'Vietnam');
        //diffrent phone
        TestDataForLeadFlow.createContact('test@test.com','(081) 987-6525',parentRecordTypeId,acc.id,Date.today().addDays(3),null,'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','0709799559',Date.today(),null,'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        //system.assertEquals(leadList[0].Existing_Contact__r.id ,matchContact.id);
        
    }
    
    @IsTest
    public static void when_leadMatchesOnDueDate_duplicateResult_shouldBe_ExistingPregnancy(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,null,Date.today(),'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','0709799559',null,Date.today(),'Vietnam');
        Test.stopTest(); 
        List<Lead> leadList = [select id,Existing_Contact__r.id,Duplicate_Result__c from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        //system.assertEquals(leadList[0].Existing_Contact__r.id ,matchContact.id);
        //due date match
        //system.assertEquals(leadList[0].Duplicate_Result__c , 'Existing parent, existing pregnancy');
    }
    
    @IsTest
    public static void when_leadDoesNotMatchOnDueDate_duplicateResult_shouldBe_NewPregnancy(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',parentRecordTypeId,acc.id,null,Date.today().addYears(-1),'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','(081) 987-6524',null,Date.today(),'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id,Duplicate_Result__c from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        system.assertEquals(leadList[0].Existing_Contact__r.id ,matchContact.id);
        // Lead duedate greater than (contact duedate +8 months)
        system.assertEquals(leadList[0].Duplicate_Result__c , 'Existing parent, new pregnancy');
    }
    
    @IsTest
    public static void when_leadMatchesOnDOB_duplicateResult_shouldBe_ExistingChild(){
        
         Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',childRecordTypeId,acc.id,Date.today().addYears(-1),null,'Vietnam');
        Contact matchContact1 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6525',childRecordTypeId,acc.id,Date.today(),null,'Vietnam');
         //should match - parent recordtype
        Contact matchContact2 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6526',parentRecordTypeId,acc.id,Date.today(),null,'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','(081) 987-6526',Date.today(),null,'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id,Duplicate_Result__c from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        //Lead Birthdate matchs with contact birthdate
        system.assertEquals(leadList[0].Duplicate_Result__c , 'Existing parent, existing child');
    }
    
    @IsTest
    public static void when_leadDoesNotMatchOnDOB_duplicateResult_shouldBe_NewChild(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6524',childRecordTypeId,acc.id,Date.today().addYears(-1),null,'Vietnam');
        Contact matchContact1 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6525',childRecordTypeId,acc.id,Date.today().addYears(-2),null,'Vietnam');
        //should match - parent recordtype
        Contact matchContact2 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6526',parentRecordTypeId,acc.id,Date.today(),null,'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','(081) 987-6526',Date.today(),null,'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id,Duplicate_Result__c from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        //Lead Birthdate greater than (All contact'ss closest Birthdate +8 months)
        system.assertEquals(leadList[0].Duplicate_Result__c , 'Existing parent, new child');
    }
    
    @IsTest
    public static void when_leadDoesNotMatchOnAnything_duplicateResult_shouldBe_QualifiedNewLead(){
        
        Id parentRecordTypeId = TestDataForLeadFlow.contactParentRecordTypeId();
        Id childRecordTypeId = TestDataForLeadFlow.contactChildRecordTypeId();
        Account acc = TestDataForLeadFlow.createAccount();
        Contact matchContact = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6529',childRecordTypeId,acc.id,Date.today().addYears(-1),null,'Vietnam');
        Contact matchContact1 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6525',childRecordTypeId,acc.id,Date.today().addMonths(-2),null,'Vietnam');
        Contact matchContact2 = TestDataForLeadFlow.createContact('test@test.com','(081) 987-6526',parentRecordTypeId,acc.id,Date.today(),null,'Vietnam');
        Test.startTest();
        Lead led = TestDataForLeadFlow.createLead('diffrent@test.com','(081) 987-6524',Date.today(),null,'Vietnam');
        Test.stopTest();
        List<Lead> leadList = [select id,Existing_Contact__r.id,Duplicate_Result__c from Lead where id=:led.id];
        
        system.assert(leadList.size()>0);
        //No matching contact found
        system.assertEquals(leadList[0].Duplicate_Result__c , 'Qualified - New Lead');
    }
}
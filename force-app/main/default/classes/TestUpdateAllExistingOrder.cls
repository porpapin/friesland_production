/* Created Date: 9 Sept 2019
 * Test Class for UpdateAllExistingOrder
 */

@isTest
private  class TestUpdateAllExistingOrder {
    @testSetup 
    static void setup() {   
        List<Lead> leads = new List<Lead>();
        Product2 product = new Product2(Name='test');
        insert product;
        Datetime dueDate = Datetime.newInstance(2019, 1, 15);
        //String leadRecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId(); 
        for (Integer i=0;i<10;i++) {
            leads.add(new Lead(
                RecordTypeId = RecordTypeUtility.LeadMYRT,
                FirstName='dsfsdfsdfdsfds',
                LastName='dsfsdfsdfdsfds',
                Company='asfasfasf',
                Email='safasf@dsfvfdsfds.com',
                ProductPurchase__c = product.Id,
                ActivityType__c = 'Purchase',
                Child_Due_Date__c = dueDate.date(),
                MobilePhone = '0938237482'
            ));
        }
        
        insert leads;
        
        List<Order> orders = new List<Order>();
        for (Integer i=0;i<10;i++) {
            orders.add(new Order(RecordTypeId = RecordTypeUtility.orderMYRT, LeadId__c=leads[i].Id,Product__c = product.Id, Type = 'Purchase'));
        }
        //insert orders;
        
    }
    @isTest
    static void test() {        
        Test.startTest();
        UpdateAllExistingOrder uao = new UpdateAllExistingOrder();
        Id batchId = Database.executeBatch(uao);
        Test.stopTest();
        // after the testing stops, assert records were updated properly
        System.assertNotEquals(null, [select Lead_Source__c from Order ]);
    }
    
}
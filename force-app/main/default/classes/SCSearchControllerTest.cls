@isTest
global class SCSearchControllerTest {
    
    @isTest
    public static void doSearchTest(){
        SCSearchController SCSearch = new SCSearchController();
        SCSearch.doSearch();
    }
    
    @isTest
    global static void getCustomerByPhoneTest(){
        Contact con = new Contact();
        con.FirstName = 'TestFirstName';
        con.LastName = 'TestLastName';
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        SCSearchController.getCustomerByPhone(con.Phone);
    }
    
    @isTest
    global static void getContactsTest(){
        Contact con = new Contact();
        con.FirstName = 'TestFirstName';
        con.LastName = 'TestLastName';
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        SCSearchController.getContacts(con.FirstName);
    }
    
    @isTest
    global static void getAccountByContactTest(){
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.CurrencyIsoCode = 'USD';
        insert acc;
        
        Contact con = new Contact();
        con.FirstName = 'TestFirstName';
        con.LastName = 'TestLastName';
        con.CurrencyIsoCode = 'USD';
        con.AccountId = acc.Id;
        insert con;
        
        SCSearchController.getAccountByContact(con.Id);
    }
    
    
    @isTest
    global static void getCasesByContactIdTest(){
        Contact con = new Contact();
        con.FirstName = 'TestFirstName';
        con.LastName = 'TestLastName';
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        Case cse = new Case();
        cse.Status = 'New';
        cse.CurrencyIsoCode = 'USD';
        cse.ContactId = con.Id;
        insert cse;
        
        SCSearchController.getCasesByContactId(con.Id);
    }
    
    @isTest
    global static void getCasesByAccountIdTest(){
        Account acc = new Account();
        acc.Name = 'TestAccount';
        acc.CurrencyIsoCode = 'USD';
        insert acc;
        
        Case cse = new Case();
        cse.Status = 'New';
        cse.CurrencyIsoCode = 'USD';
        cse.AccountId = acc.Id;
        insert cse;
        
        SCSearchController.getCasesByAccountId(acc.Id);
    }
    
    @isTest
    global static void getCasesTest(){
        Case cse = new Case();
        cse.Status = 'New';
        cse.CurrencyIsoCode = 'USD';
        insert cse;
        
        SCSearchController.getCases();
    }
    
    @isTest
    global static void getCallHistoryByContactIdTest(){
        Contact con = new Contact();
        con.FirstName = 'TestFirstName';
        con.LastName = 'TestLastName';
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        Task tk = new Task();
        tk.Subject = 'Call';
        tk.OwnerId = UserInfo.getUserId();
        tk.CurrencyIsoCode = 'USD';
        tk.Priority = 'Normal';
        tk.Status = 'Open';
        tk.WhoId = con.Id;
        tk.Type = 'Call';
        insert tk;
        
        SCSearchController.getCallHistoryByContactId(con.Id);
    }
    
    @isTest
    global static void getCallHistoryTest(){
        Task tk = new Task();
        tk.Subject = 'Call';
        tk.OwnerId = UserInfo.getUserId();
        tk.CurrencyIsoCode = 'USD';
        tk.Priority = 'Normal';
        tk.Status = 'Open';
        tk.Type = 'Call';
        insert tk;
        
        SCSearchController.getCallHistory();
    }
    
    @isTest
    global static void getCallMissTest(){
        Task tk = new Task();
        tk.Subject = 'Call';
        tk.OwnerId = UserInfo.getUserId();
        tk.CurrencyIsoCode = 'USD';
        tk.Priority = 'Normal';
        tk.Status = 'Open';
        tk.Type = 'Call';
        tk.Call_Type__c = 'Inbound';
        tk.Connected_Time__c = null;
        insert tk;
        
        SCSearchController.getCallMiss();
    }

}
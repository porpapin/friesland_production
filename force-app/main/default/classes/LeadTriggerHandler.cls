/*
* Name : LeadTriggerHandler
* Created By : Sakshi Arora
* Description: Trigger Handler for LeadTrigger
* Created Date: [10/31/2018]
* Task : T-746253
* 
* */
public class LeadTriggerHandler implements ITriggerHandler {
    
    
    public Boolean isDisabled(){                
        /*
         *New version using Custom Metadata Types 
         */
        TriggerStatus__mdt triggerStatus = [SELECT Disabled__c FROM TriggerStatus__mdt WHERE label = 'LeadTriggerHandler'][0];
        return triggerstatus.Disabled__c; 
        
        
    }
    
    public void beforeInsert(List<SObject> newItems){
        
        List<Lead> leads = (List<Lead>) newItems;
        ProductLead productLead = new ProductLead();
        productLead.populateProductOnLead(leads, null);
        
    }
    
    public void beforeUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
        Map<Id,Lead> leadsMap = (Map<Id,Lead>) newItems;
        
        
        ProductLead productLead = new ProductLead();
        productLead.populateProductOnLead(leadsMap.values(), (Map<Id,Lead>) oldItems);
    
    }
    
    public void beforeDelete(Map<Id,SObject> oldItems){
        System.debug('executing before delete');
    }
    
    public void afterInsert(Map<Id,SObject> newItems){
        
        Map<Id,Lead> newLeadMap = (Map<Id,Lead>)newItems;
        
        executeLeadDuplicateCategorizationFlow(newLeadMap);
                
    }
    
    public void afterUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
        Map<Id,Lead> newLeadMap = (Map<Id,Lead>)newItems;
        Map<Id,Lead> oldLeadMap= (Map<Id,Lead>)oldItems;
        
        
        LeadDedupeActions lda = new LeadDedupeActions();   
        lda.fireActionsForDedupeResult(newLeadMap,oldLeadMap); 
        
    }
    
    public void afterDelete(Map<Id,SObject> oldItems){
        System.debug('executing after delete');
    }
    
    public void afterUndelete(Map<Id,SObject> oldItems){
        System.debug('executing after undelete');
    }

    
    private void executeLeadDuplicateCategorizationFlow(Map<Id,Lead> leadsMap){
        Map<Id,Lead> leadChildDobvalidationMap = new Map<Id,Lead>();
        if(!leadsMap.isEmpty()){
            for(Lead ldsObjValidatingChildDOB : leadsMap.Values()){
                if(ldsObjValidatingChildDOB.child_dob__c != null){
                    ldsObjValidatingChildDOB.child_dob__c.addMonths(10);
                    leadChildDobvalidationMap.put(ldsObjValidatingChildDOB.Id,ldsObjValidatingChildDOB);
                }
                else
                {
                    leadChildDobvalidationMap.put(ldsObjValidatingChildDOB.Id,ldsObjValidatingChildDOB);
                }
            }
            if(!leadChildDobvalidationMap.isEmpty()){
                LeadTriggerHandlerHelper.validateLeadsWithContact(leadChildDobvalidationMap.values());
            }
        }
    /*
    
        
          Set<Id> leadsIds = leadsMap.keySet();
        
        if(leadsIds != null && leadsIds.size() > 0){
            try{
                // Updated By Naresh Added Global Flow Calling
                //Flow.Interview flow = new Flow.Interview.LeadFlow(new map<String,Object> 
                  //                                                {'vLeadId' =>new List<Id>(leadsIds)[0]}); 
                Id firstLeadId = (new list<Id>(leadsIds))[0];
                system.debug('firstLeadId--->'+firstLeadId);
                Lead leadtoConvert = leadsMap.get(firstLeadId);
                system.debug('leadtoConvert--->'+leadtoConvert);
                Flow.Interview flow;
                if(leadtoConvert.child_dob__c != null){
                    Datetime TenMonthsDate = leadtoConvert.child_dob__c.addMonths(10);
                    system.debug('TenMonthsDate---->'+TenMonthsDate);
                    flow = new Flow.Interview.GlobalLeadDuplicateCategorizationFlow(new map<String,Object> 
                                                                  {'vLeadId' => new List<Id>(leadsIds)[0],
                                                                   'varTenMonthsDate' => TenMonthsDate});   
                } else {
                    flow = new Flow.Interview.GlobalLeadDuplicateCategorizationFlow(new map<String,Object> 
                                                                     {'vLeadId' =>new List<Id>(leadsIds)[0]});  
                } 
                flow.start();
            }catch(Exception e){
                //we'll figure this out
            }
        
    }*/

        
    }
    
    
    
    
}
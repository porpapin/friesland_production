/*
* Name : TriggerDispatcher
* Created By : Sakshi Arora
* Purpose: To making sure all of the applicable methods on trigger handler are called.
* Created Date: [10/31/2018]
* Task : T-746253
* 
* */
public class TriggerDispatcher{
    
    public static void run(ITriggerHandler handler){
        
        if(handler.isDisabled()){
            return;
        }
        
        //handle all before logic
        if(Trigger.IsBefore){
            
            System.debug('IsBefore');
            if(Trigger.IsInsert){
                System.debug('IsInsert');
                handler.beforeInsert(trigger.new);
            }
            
            if(Trigger.IsUpdate){
                System.debug('IsUpdate');
                handler.beforeUpdate(trigger.newMap,trigger.oldMap);
            }
            
            if(Trigger.IsDelete){
                handler.beforeDelete(trigger.oldMap);
            }
            
            
        }
        
        //handle all after logic
        
        if(Trigger.isAfter){
            
            System.debug('isAfter');
            if(Trigger.isInsert){
                System.debug('isInsert');
                RecordTypeUtility.checker = false;               
                handler.afterInsert(trigger.newMap);
                
            }
            
            if(Trigger.isUpdate){
                System.debug('isUpdate oldMap'+trigger.oldMap);
                System.debug('isUpdate newMap'+trigger.newMap);
                handler.afterUpdate(trigger.newMap,trigger.oldMap);
                
            }
            
            if(Trigger.IsDelete){
                handler.afterDelete(trigger.oldMap);
            }
            
            if(Trigger.IsUndelete){
                handler.afterUndelete(trigger.oldMap);
            }
            
        }
    }
}
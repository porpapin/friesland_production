@isTest 
public class CreateTaskAndAssignToPResentAgents_test 
{
    static testMethod void CreateTaskAndAssignToPresentAgents() 
    {
        test.startTest();
        User usr = new User(
            ProfileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator'].Id,
            LastName = 'last',
            Email = 'nshiwani@appirio.com',
            Username = 'nshiwani@appirio.com' + System.currentTimeMillis(),
            CompanyName = 'TEST',
            Title = 'title',
            Alias = 'alias',
            TimeZoneSidKey = 'America/Los_Angeles',
            EmailEncodingKey = 'UTF-8',
            LanguageLocaleKey = 'en_US',
            LocaleSidKey = 'en_US'
        );
        
        insert usr;
        AgentsPresence__c ag = new AgentsPresence__c();
        ag.IsPresent__c	 =true;
        ag.User__c=usr.id;
        insert ag;
        
        Account testAccount = new Account();
        testAccount.Name='Test Account' ;
        insert testAccount;
        
        Contact cont = new Contact();
        cont.FirstName='Test';
        cont.LastName='Test';
        cont.Accountid= testAccount.id;
        insert cont;
        
        Campaign camp1 = new Campaign(Name = 'Test Campaign 1', IsActive = True);
        insert camp1;
        
        CampaignMember cm = new CampaignMember();
        cm.CampaignId=camp1.id;
        cm.ContactId=cont.id;
        cm.Status='Sent';
        insert cm;
        
        Task myTask = new Task();
        myTask.Subject='This is subject';
        myTask.Priority='Normal';
        myTask.Status='Not Started';
        myTask.OwnerId =usr.id;
        //myTask.WhoId='0036F000030w7hJ';
        insert myTask;   
        test.stopTest();
    }
}
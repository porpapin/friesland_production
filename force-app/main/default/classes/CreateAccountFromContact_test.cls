@isTest
public with sharing class CreateAccountFromContact_test{
	@isTest
	public static void runTest(){
        test.startTest();
		String firstname = 'first';
		String lastname = 'last';
		String email = 'firstlast@test.com';
        String Recordtype =Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_MY').getRecordTypeId();
        
		
		//Create contact
        Contact c = new Contact(firstname=firstname, lastname=lastname, email=email, recordtypeid=Recordtype);
        system.debug('Contact Record Type >>'+  c);
        insert c;
        
        //Verify account
        for(Contact con : [select id, accountid, firstname, lastname, email from Contact where id =: c.Id]){
            c = con;
        }
        system.debug('c---->'+c);
        Account a;
        for(Account acnt : [select id, name from Account where id =: c.accountId]){
            a = acnt;
        }
        
		system.assertEquals(firstname + ' ' + lastname, a.Name);
        test.stopTest();
	}
}
/*
* Name : DueDateToContactSchedule
* Created By : Sakshi Arora
* Created Date: [11/16/2018]
* Task : T-751749
* Description : Schedulable class for DueDateToContactBatch
* */
global class DueDateToContactSchedule implements Schedulable{

    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        DueDateToContactSchedule SC = new DueDateToContactSchedule(); 
        return System.schedule('Due date to birth date', sched, SC);
    }

    global void execute(SchedulableContext sc) {
        System.debug('start batch');
        DueDateToContactBatch b1 = new DueDateToContactBatch();
        ID batchprocessid = Database.executeBatch(b1,200);           
    }
}
/*
* Name : LeadDedupeActions_VNTest
* Created By : pw
* Description : Test class for LeadDedupeActions_VN
* */  

@IsTest
global class LeadDedupeActions_VNTest {
    
    public static testMethod void testVNLeadNewLeadSample(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadSample1(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            l.ActivityType__c='Sample';
            
            l2.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadSample2(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.ActivityType__c='Sample';
            
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            l2.ActivityType__c='Sample';
            
            l2.child_dob__c = Date.today().addMonths(-12);
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadSample3(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.ActivityType__c='Sample';
            
            l.Child_Due_Date__c = system.today().addMonths(2);
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadSample4(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.Child_Due_Date__c = system.today().addMonths(2);
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            l.ActivityType__c='Sample';
            
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            //l2.child_dob__c = Date.today().addMonths(-12);
            l2.Child_Due_Date__c = system.today().addMonths(2);
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            l2.ActivityType__c='Sample';
            
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadPurchase(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Purchase';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadPurchase1(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Purchase';
            
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            l.ActivityType__c='Purchase';
            
            l2.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadPurchase2(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.ActivityType__c='Purchase';
            
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            l2.ActivityType__c='Purchase';
            
            l2.child_dob__c = Date.today().addMonths(-12);
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadPurchase3(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.ActivityType__c='Purchase';
            
            l.Child_Due_Date__c = system.today().addMonths(2);
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;

            Test.stopTest();
        }
        
    }
    
    public static testMethod void testVNLeadNewLeadPurchase4(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
            insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.Child_Due_Date__c = system.today().addMonths(2);
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            l.ActivityType__c='Purchase';
            
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '0327656789';
            l2.Campaign__c = c.id;
            //l2.child_dob__c = Date.today().addMonths(-12);
            l2.Child_Due_Date__c = system.today().addMonths(2);
            l2.Country__c = 'Vietnam';
            l2.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            l2.ActivityType__c='Purchase';
            
            Product2 prod1 = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    

}
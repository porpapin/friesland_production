@IsTest
public class CaseOwnerManagerTest {
    
    @TestSetup
    public static void createTestData(){
        
        Profile adminProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
        
        User caseOwner = SObjectUtility.createTestUser(adminProfile.Id, 'case', 'owner');
        User caseOwnerManager = SObjectUtility.createTestUser(adminProfile.Id, 'case', 'ownerManager');
        
        insert caseOwner;
        insert caseOwnerManager;
        
        User newCaseOwner = [SELECT Id, Manager.Id FROM User WHERE Id = :caseOwner.Id];
        
        newCaseOwner.ManagerId = caseOwnerManager.Id;
        
        update newCaseOwner;
        
        System.debug(JSON.serializePretty(newCaseOwner));
        
      
        
    }
    
    @IsTest
    public static void testReassignToManager(){
        
        
        User caseOwner = [SELECT Id FROM User WHERE LastName = 'owner' LIMIT 1];
        Contact testContact = new Contact(lastName = 'test');
        insert testContact;
        Case newCase = new Case();
        newCase.OwnerId = caseOwner.Id;
        newCase.ContactId = testContact.Id;
        insert newCase;
        
        List<Case> cases = [SELECT Id, Home_Visit_SLA_Missed__c FROM Case LIMIT 1];
        
        for(Case c : cases){
            c.Home_Visit_SLA_Missed__c = true;
        }
        
        update cases;
        
    }
    
	@IsTest
    public static void testDeleteUndelete(){
        
        
        User caseOwner = [SELECT Id FROM User WHERE LastName = 'owner' LIMIT 1];
        Contact testContact = new Contact(lastName = 'test');
        insert testContact;
        Case newCase = new Case();
        newCase.OwnerId = caseOwner.Id;
        newCase.ContactId = testContact.Id;
        insert newCase;
        
        
        delete newCase;
        undelete newCase;
        
    }
}
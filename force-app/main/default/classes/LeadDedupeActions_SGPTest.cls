@isTest
public class LeadDedupeActions_SGPTest {
    
    public static testMethod void testSGPLeadNewLeadSampleFirstLeadWithDOB(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='1';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            l.Product__c = prod.id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            insert l;
            
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleExistingChild(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='1';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.NameOfChild__c ='ChildFirstName';
            l1.LastNameOfChild__c='LastNameofChild';
            l1.MobilePhone = '87654321';
            l1.Campaign__c = cam.id;
            l1.child_dob__c = Date.today().addMonths(-2);
            l1.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Singapore';
            l1.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l1.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            //l1.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l1.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l1;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleExistingChildNewOrder(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='1';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            //system.debug('stage ?'+camPro);
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            insert l;
            Contact con = new Contact();
            con = [SELECT AccountId from Contact where Email=:l.Email];
            Account acc = new Account();
            acc = [SELECT Id from Account where id=:con.AccountId];
            con = [SELECT Id,RecordtypeId from Contact where Accountid=:acc.Id and RecordtypeId=:RecordTypeUtility.contactChildSGRT];
            Order ord = [SELECT Id,EffectiveDate,Child_Contact__c from Order where Child_Contact__c=:con.Id];
            ord.EffectiveDate = ord.EffectiveDate.addDays(-2);
            
            Lead l1 = new Lead(firstname='Testtttt',lastname='Lastttttt', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.NameOfChild__c ='ChildFirstName';
            l1.LastNameOfChild__c='LastNameofChild';
            l1.MobilePhone = '87654321';
            l1.Campaign__c = cam.id;
            l1.child_dob__c = Date.today().addMonths(-8);
            l1.ActivityType__c='Sample';
            l1.Country__c = 'Singapore';
            l1.Company = 'Singapore';
            l1.Email = 'test@vietnam.com';
            
            l1.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            insert l1;
            
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleNewChild(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='1';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            //l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.NameOfChild__c ='ChildFirstName';
            l1.LastNameOfChild__c='LastNameofChild';
            l1.MobilePhone = '87654321';
            l1.Campaign__c = cam.id;
            l1.child_dob__c = Date.today().addMonths(7);
            l1.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Singapore';
            l1.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l1.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            //l1.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l1.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l1;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleFirstLeadWithCDD(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='0';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.Child_Due_Date__c = Date.today().addMonths(2);
            //l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            //l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleExistingPregnancy(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='0';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.Child_Due_Date__c = Date.today().addMonths(2);
            //l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '87654321';
            l2.Campaign__c = cam.id;
            //l2.child_dob__c = Date.today().addMonths(-2);
            l2.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Singapore';
            l2.Company = 'Singapore';
            l2.Child_Due_Date__c = Date.today().addMonths(2);
            
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
   
    public static testMethod void testSGPLeadNewLeadSampleNewPregnancyNotConverted(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='0';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.Child_Due_Date__c = Date.today().addMonths(2);
            //l.child_dob__c = Date.today().addMonths(-2);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            Lead l2 = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l2.email = 'email@test.cmo';
            l2.NameOfChild__c ='ChildFirstName';
            l2.LastNameOfChild__c='LastNameofChild';
            l2.MobilePhone = '87654321';
            l2.Campaign__c = cam.id;
            //l2.child_dob__c = Date.today().addMonths(-2);
            l2.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l2.Country__c = 'Singapore';
            l2.Company = 'Singapore';
            l.Child_Due_Date__c = Date.today().addMonths(10);
            
            //l.MobilePhone = '81045678';
            l2.Email = 'test@vietnam.com';
            //Product2 prod = SobjectUtility.createProduct();
            l2.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l2.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l2;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testSGPLeadNewLeadSampleFirstLeadWithDOBMT83(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Product2 prod = new Product2();
            prod.Name = 'Frisomum Gold';
            prod.IsActive = true;
            prod.Country__c = 'Singapore';
            prod.Family ='GUM';
            prod.producttype__c = 'C_Child';
            //prod.CurrencyIsoCode = 'SGD - Singapore Dollar';
            prod.ProductCategory__c = 'Sample';
            prod.Stage__c ='1';
            insert prod;
            
            PricebookEntry pbe = new PricebookEntry();
            pbe.IsActive = true;
            pbe.Product2Id = prod.Id;
            pbe.Pricebook2Id = Test.getStandardPricebookId();
            pbe.UnitPrice = 25.00;
            insert pbe;
            
            pbe = [Select CurrencyIsoCode from PricebookEntry where id=:pbe.Id ];
            
            system.debug('Currency>'+pbe.CurrencyIsoCode);
            
            system.debug('Price Book Entry: '+pbe);
            //system.debug('Product stage ????'+prod.Stage__c);
            
            Campaign cam = new Campaign(Name='Test');
            cam.RecordTypeId =Schema.SObjectType.Campaign.getRecordTypeInfosByDeveloperName().get('Campaign_SG').getRecordTypeId();
            cam.IsActive = true;
            insert cam;
            //system.debug('campaign:'+cam);
            Campaign_Product__c camPro = new Campaign_Product__c();
            camPro.Product__c = prod.Id;
            camPro.Campaign__c = cam.Id;
            
            //camPro.CurrencyIsoCode = 'SGD - Singapore Dollar';
            insert camPro;
            
            camPro = [SELECT id,Stage__c,Campaign__c,Product__c from Campaign_Product__c where id=:camPro.Id];
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '87654321';
            l.Campaign__c = cam.id;
            l.child_dob__c = Date.today().addMonths(-85);
            l.ActivityType__c='Sample';
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            l.Product__c = prod.id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            insert l;
            
            Test.stopTest();
        }
        
    }
    



}
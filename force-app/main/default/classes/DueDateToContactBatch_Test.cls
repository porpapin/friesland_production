/*
* Name : DueDateToContactBatch_Test
* Created By : Sakshi Arora
* Created Date: [11/16/2018]
* Task : T-751749
* Description : Test class for DueDateToContactBatch
* */
@isTest
private class DueDateToContactBatch_Test {
    static testmethod void test(){
        List<Account> accounts = new List<Account>();
        List<Contact> contacts = new List<Contact>();
        // insert 10 accounts
        for (Integer i=0;i<10;i++) 
        {
            accounts.add(new Account(name='Account '+i,billingcity='New York', billingcountry='USA'));
        }
        insert accounts;
        // find the account just inserted. add contact for each
        for (Account account : [select id from account]) 
        {
            contacts.add(new Contact(firstname='first',lastname='Last', Street__c='TestSTreet',Province__c='Quảng Nam',District__c='Đông Giang',Region__c='Central',accountId=account.id, Due_Date__c = System.today()));
        }
        insert contacts;
                 
        Test.startTest();
        DueDateToContactSchedule.scheduleMe();
        //DueDateToContactBatch dueDateBatch = new DueDateToContactBatch();
        //Id batchId = Database.executeBatch(dueDateBatch);
        

        DueDateToContactBatch dueDateBatch = new DueDateToContactBatch();
        Database.QueryLocator ql = dueDateBatch.start(null);
        dueDateBatch.execute(null,contacts);
        dueDateBatch.Finish(null);
        
        Test.stopTest();
        // after the testing stops, assert records were updated properly
       // System.assertEquals(10, [select count() from contact where LastName LIKE '%Child Last%']);
        
    }
    
    
        
       
    
}
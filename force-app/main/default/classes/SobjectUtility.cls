/*
* Name : Utility
* Created By : Sakshi Arora
* Description: Utility class to keep track of common Methods
* Created Date: [11/02/2018] 
*
* C-00253748 3/18/19 NCarson - Leads need first names
* Updated Navin T-794896: Updated RecordType
* */
public with sharing class SobjectUtility {
    
    public static Boolean isNewOrChanged(Sobject obj, String fldName, Map<Id, Sobject> oldMap) {
        if (oldMap != null && obj != null && fldName != null) {
            return ((obj.get(fldName) != oldMap.get((Id)obj.get('id')).get(fldName) ? true : false));
        }
        return true;
    }
    
    public static User createTestUser(Id profileID, String fName, String lName){
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(  firstname = fName,
                              lastName = lName,
                              email = uniqueName + '@test' + orgId + '.org',
                              Username = uniqueName + '@test' + orgId + '.org',
                              EmailEncodingKey = 'ISO-8859-1',
                              Alias = uniqueName.substring(18, 23),
                              TimeZoneSidKey = 'America/Los_Angeles',
                              LocaleSidKey = 'en_US',
                              LanguageLocaleKey = 'en_US',
                              ProfileId = profileID);
        return tuser;
    }
    
    
    public static Id contactParentRecordTypeId()
    {
        //Navin T-794896: Fetch RT by Developer name
        //return Schema.SObjectType.contact.getRecordTypeInfosByName()
        //.get('Parent').getRecordTypeId();
        return Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Parent').getRecordTypeId();
    }
    public static Id contactChildRecordTypeId()
    {
        //Navin T-794896: Fetch RT by Developer name
        //return Schema.SObjectType.contact.getRecordTypeInfosByName()
        //.get('Child').getRecordTypeId();
        return Schema.SObjectType.contact.getRecordTypeInfosByDeveloperName().get('Child').getRecordTypeId();
    }
    public static Contact createContact(string email,string phone,Id recordTypeId,Id accountId,Date birthDate,Date dueDate,string country)
    {
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = email,
            MobilePhone = phone); 
        c.RecordTypeId = recordTypeId;
        c.accountid = accountId;
        c.Birthdate = birthDate;
        c.Due_Date__c = dueDate;
        c.Country__c = country;
        c.Street__c='Street';
        insert c; 
        return c;
        
    }
    public static Product2 createProduct()
    {
        Id priceBook = Test.getStandardPricebookId();
        Product2 prod = new Product2();
        prod.Name = 'Frisomum Gold';
        prod.IsActive = true;
        prod.Country__c = 'Vietnam';
        insert prod;
        return prod;
    }
    
    public static Product2 createProductMYandSG(String country)
    {
        Id priceBook = Test.getStandardPricebookId();
        Product2 prod = new Product2();
        prod.Name = 'Frisomum Gold';
        prod.IsActive = true;
        prod.Country__c = country;
        insert prod;
        return prod;
    }
    
    public static PricebookEntry createPricebookEntry(Product2 prod)
    {
        Id priceBook = Test.getStandardPricebookId();
        
        PricebookEntry pbe  = new PricebookEntry(Pricebook2Id=priceBook, Product2Id=prod.Id,
                                                 UnitPrice=0.00, IsActive=true, UseStandardPrice=false);
        insert pbe;
        return pbe;
    }
    
    
    public static Lead createLead(string email,string phone,Date birthDate,Date dueDate,String country,string a, string b)
    {	
        Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = email;
        l.MobilePhone = phone;
        l.child_dob__c = birthdate;
        l.Child_Due_Date__c = dueDate;
        l.Country__c = country;
        l.Product_Requested__c = createProduct().Id;
        l.NameOfChild__c = a;
        l.LastNameOfChild__c = b;
        insert l;
        return l;
    }
    
    public static Lead createLeadVN(string email,string phone,Date birthDate,Date dueDate,String country)
    {	
        Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = email;
        l.MobilePhone = phone;
        l.child_dob__c = birthdate;
        l.Child_Due_Date__c = dueDate;
        l.NameOfChild__c = 'fname';
        l.LastNameOfChild__c = 'lname';
        l.Country__c = country;
        l.Product_Requested__c = createProduct().Id;
        l.Duplicate_Result__c = 'Qualified - New Lead';
        insert l;
        return l;
    }
    
    public static Lead createLeadMYandSG(string email,string phone,Date birthDate,Date dueDate,String country,String recordName,Boolean isBoolean)
    {	
        Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = email;
        l.MobilePhone = phone;
        l.child_dob__c = birthdate;
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = country;
        l.Product_Requested__c = createProductMYandSG('Malaysia').Id;
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get(recordName).getRecordTypeId();
        l.Company = country;
        if(isBoolean){
        	insert l;
        }
        return l;
    }
    
    public static Account createAccount()
    {
        Account acct = new Account(Name='accountName');
        insert acct;
        return acct;
    }
    
    public static Order createOrder(Id accountId,Id contactId,String type,String status)
    {
        Order order = new Order();
        order.AccountId = accountId;
        order.Contact__c = contactId;
        order.Type = type;
        order.EffectiveDate = System.today();
        order.Status = status;
        insert order;
        return order;
    }
    
    //Navin T-794896: Created portal user for test class coverage
    public static User portalUserForVietnam(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs ( thisUser ) {
            UserRole ur = [Select Id from UserRole Where Name = 'Vietnam Country Head'];
            Profile p = [select Id,name from Profile where Name = 'FrieslandCampina CRM Manager' limit 1];
            
            User newUser = new User(
                profileId = p.id,
                UserRoleId = ur.id,
                username = 'abc@xyz12321.com',
                email = 'abc@xyz12321.com',
                emailencodingkey = 'UTF-8',
                localesidkey = 'en_US',
                languagelocalekey = 'en_US',
                timezonesidkey = 'America/Los_Angeles',
                alias='nuser',
                firstname='abc',
                lastname='xyz',
                country__c = 'Vietnam'
            );
            insert newUser; 
            return newUser;
        }
        return null;
    }
    
    //Navin T-794896: Created portal user for test class coverage
    public static User portalUserForMalaysia(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs ( thisUser ) {
            UserRole ur = [Select Id from UserRole Where Name = 'Malaysian Country Head'];
            Profile p = [select Id,name from Profile where Name = 'MY Operations' limit 1];
            
            User newUser = new User(
                profileId = p.id,
                UserRoleId = ur.id,
                username = 'abc@xyz12321.com',
                email = 'abc@xyz12321.com',
                emailencodingkey = 'UTF-8',
                localesidkey = 'en_US',
                languagelocalekey = 'en_US',
                timezonesidkey = 'America/Los_Angeles',
                alias='nuser',
                firstname='abc',
                lastname='xyz',
                country__c = 'Malaysia'
            );
            insert newUser; 
            return newUser;
        }
        return null;
    }
    
    //Navin T-794896: Created portal user for test class coverage
    public static User portalUserForSingapore(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        
        System.runAs ( thisUser ) {
            UserRole ur = [Select Id from UserRole Where Name = 'Singapore Country Head'];
            Profile p = [select Id,name from Profile where Name = 'SG Operations' limit 1];
            
            User newUser = new User(
                profileId = p.id,
                UserRoleId = ur.id,
                username = 'abc@xyz12321.com',
                email = 'abc@xyz12321.com',
                emailencodingkey = 'UTF-8',
                localesidkey = 'en_US',
                languagelocalekey = 'en_US',
                timezonesidkey = 'America/Los_Angeles',
                alias='nuser',
                firstname='abc',
                lastname='xyz',
                country__c = 'Singapore'
            );
            insert newUser; 
            return newUser;
        }
        return null;
    }
}
/*
 * Name : TestDataForLeadFlow
 * Created By : Naresh Kumar
 * Description: Class is used to prepare test data for LeadDuplicateFlow_Tests test class
 * Created Date: [13 Nov 2018]
 * Task : T-749469
 * 
 * */
public class TestDataForLeadFlow {
    
    public static Id contactParentRecordTypeId()
    {
        return Schema.SObjectType.contact.getRecordTypeInfosByName()
            .get('Parent').getRecordTypeId();
    }
    public static Id contactChildRecordTypeId()
    {
        return Schema.SObjectType.contact.getRecordTypeInfosByName()
            .get('Child').getRecordTypeId();
    }
    public static Contact createContact(string email,string phone,Id recordTypeId,Id accountId,Date birthDate,Date dueDate,string country)
    {
        
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = email,
            MobilePhone = phone); 
        c.RecordTypeId = recordTypeId;
        c.accountid = accountId;
        c.Birthdate = birthDate;
        c.Due_Date__c = dueDate;
        c.Country__c = country;
        insert c; 
        return c;
        
    }
    
    public static Lead createLead(string email,string phone,Date birthDate,Date dueDate,String country)
    {
        Lead l = new Lead(lastname='11', company='11');
        l.email = email;
        l.MobilePhone = phone;
        l.child_dob__c = birthdate;
        l.Child_Due_Date__c = dueDate;
        l.Country__c = country;
        insert l;
        return l;
    }
    
    public static Account createAccount()
    {
        Account acct = new Account(Name='accountName');
        insert acct;
        return acct;
    }
    
    
}
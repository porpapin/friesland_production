/*
* Name : LeadDedupeActions_Test
* Created By : Naresh K Shiwani
* Description : Test class for LeadDedupeActions
* */  
@IsTest
public class LeadDedupeActionsTestNew {
    public static testMethod void testQualifiedNewLead(){
        Test.startTest();
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.child_dob__c = Date.today().addYears(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Qualified - New Lead';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testExistingParentNewChild(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.child_dob__c = Date.today().addYears(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Existing parent, new child';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testExistingParentExistingChild(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
        Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
        Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmo','0104567458',contactChildMYRT,acc.id,null,null,'Malaysia');
        matchContactchild.Birthdate = system.today().addYears(-2);
        update matchContactchild;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.child_dob__c = Date.today().addYears(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Existing parent, existing child';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testNewsLetterNewChild(){
        Test.startTest();
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.ActivityType__c = 'Newsletter';
        
        l.child_dob__c = Date.today().addYears(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
        l.Duplicate_Result__c = 'Existing parent, existing child';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testNewsLetterExistingParent(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
        Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
        Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmo','0104567458',contactChildMYRT,acc.id,null,null,'Malaysia');
        matchContactchild.Birthdate = system.today().addYears(-2);
        update matchContactchild;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.ActivityType__c = 'Newsletter';
        
        l.child_dob__c = Date.today().addMonths(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
        insert l;
        l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
        l.Duplicate_Result__c = 'Existing parent, existing child';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testExistingParentExistingPregnency(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
        Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
        Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmo','0104567458',contactChildMYRT,acc.id,null,null,'Malaysia');
        matchContactchild.Birthdate = system.today().addYears(-2);
        update matchContactchild;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.Child_Due_Date__c = Date.today().addYears(-1);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Existing parent, existing pregnancy';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testExistingParentNewPregnency(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
        Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
        Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmm','0104567457',contactChildMYRT,acc.id,null,null,'Malaysia');
        matchContactchild.Birthdate = system.today().addYears(-2);
        update matchContactchild;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.Child_Due_Date__c = Date.today().addYears(-1);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Existing parent, new pregnancy';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testQualifiedNewLeadSingapore(){
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
			insert c;
            Test.startTest();
            Lead l = new Lead(firstname='Test',lastname='Test', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '81045678';
            l.child_dob__c = Date.today().addYears(-2);
            l.Campaign__c = c.id;
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            Product2 prod = SobjectUtility.createProductMYandSG('Singapore');
            l.Product__c = prod.id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            //l.Product_Requested__c = createProduct().Id;
            
            
            //l.Duplicate_Result__c = 'Qualified - New Lead';
            insert l;
            //update l;
            Test.stopTest();
        } 
    }
    
    public static testMethod void testNewsLetterExistingParentSingapore(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForSingapore();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
			insert c;
            Account acc = new Account(Name='Test accountName');
            acc.RecordTypeId = RecordTypeUtility.accountMYRT;
            insert acc;
            
            Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
            Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
            //matchContact.Birthdate = system.today().addYears(-2);
            update matchContact;
            
            Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
            Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmo','0104567458',contactChildMYRT,acc.id,null,null,'Malaysia');
            matchContactchild.Birthdate = system.today().addYears(-2);
            update matchContactchild;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Singapore'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            //l.MobilePhone = '8104567898';
            l.ActivityType__c = 'Newsletter';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Singapore';
            l.Company = 'Singapore';
            l.MobilePhone = '81045678';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
            l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
    }
    
    public static testMethod void testExistingParentExistingChildNewOrder(){
        Test.startTest();
        
        Account acc = new Account(Name='Test accountName');
        acc.RecordTypeId = RecordTypeUtility.accountMYRT;
        insert acc;
        
        Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
        Contact matchContact = SobjectUtility.createContact('email@test.cmo','0104567898',contactParentMYRT,acc.id,null,null,'Malaysia');
        //matchContact.Birthdate = system.today().addYears(-2);
        update matchContact;
        
        Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
        Contact matchContactchild = SobjectUtility.createContact('emailchild@test.cmo','0104567458',contactChildMYRT,acc.id,null,null,'Malaysia');
        matchContactchild.Birthdate = system.today().addYears(-2);
        update matchContactchild;
        
        Contract contract = new Contract();
        contract.AccountId = acc.id;
        contract.Status ='Draft';
        contract.StartDate = system.today();
        contract.ContractTerm = 4;
        contract.CurrencyIsoCode = 'USD';
        insert contract;
        
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        
        Id orderRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_MY').getRecordTypeId();
        Order ord = new Order();
        ord.RecordTypeId = orderRT;
        ord.AccountId = acc.id;
        ord.Contact__c = matchContact.id;
        ord.Child_Contact__c = matchContactchild.Id;
        ord.Type ='Sample';
        ord.ContractId = contract.id;
        //ord.Sample__c = prod.Id;
        ord.CurrencyIsoCode = 'USD';
        ord.EffectiveDate = system.today();
        ord.Status = 'Draft';
        insert ord;
        
		Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
        l.email = 'email@test.cmo';
        l.MobilePhone = '0104567898';
        l.child_dob__c = Date.today().addYears(-2);
        //l.Child_Due_Date__c = dueDate;
        l.Country__c = 'Malaysia';
        l.Company = 'Malaysia';
        l.Product__c = prod.id;
        //l.Product_Requested__c = createProduct().Id;
        insert l;
        l.Duplicate_Result__c = 'Existing parent, existing child';
        update l;
        Test.stopTest();
    }
    
    public static testMethod void testNewsLetterExistingParentVietnam(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForVietnam();
        System.runAs ( thisUser ) {
            Campaign c = new Campaign(Name='Test');
			insert c;
            
            Lead l = new Lead(firstname='Test',lastname='Last', company='Vietnam'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.NameOfChild__c ='ChildFirstName';
            l.LastNameOfChild__c='LastNameofChild';
            l.MobilePhone = '0327656789';
            l.Campaign__c = c.id;
            l.child_dob__c = Date.today().addMonths(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Vietnam';
            l.Company = 'Vietnam';
            //l.MobilePhone = '81045678';
            l.Email = 'test@vietnam.com';
            Product2 prod = SobjectUtility.createProduct();
            l.Product__c = prod.id;
            //l.Product_Requested__c = createProduct().Id;
            l.RecordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
            //l.Conversion_Result__c = Label.Lead_Not_Converted_Age_Less_Than_10_Months;
            insert l;
            
            //l.Duplicate_Result__c = 'Existing parent, existing child';
            //update l;
            Test.stopTest();
        }
    }
}
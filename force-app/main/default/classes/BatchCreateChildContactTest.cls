/* Created Date: 9 Sept 2019
 * Test class for BatchCreateChildContact
 */ 

@isTest
private class BatchCreateChildContactTest {
    @testSetup 
    static void setup() {
        // Load the test data from the static resource        
		       
    }
    
    static testmethod void test() {      
        Test.startTest();        

        String parentRecordType = RecordTypeUtility.contactParentVNRT;
        String childRecordType = RecordTypeUtility.contactChildVNRT;
        
		// Parent contact without child
		Datetime dueDate = Datetime.newInstance(2017, 5, 15);
        Contact c = new Contact(firstname='firstname', lastname='lastname', email='firstlast@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date(), Created_From_Due_Date__c = false);       
        insert c;   
        
        // Parent contact with child
        Contact parentContact = new Contact(firstname='firstname', lastname='lastname', email='parent-email@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());        
        insert parentContact;
        
        Datetime childBirthDate = dueDate.addMonths(-10);
        Contact childContact = new Contact(firstname='New - ', lastname='Child', recordtypeid=childRecordType, Birthdate = childBirthDate.date(), AccountId = parentContact.AccountId, Created_From_Due_Date__c = false);        
        insert childContact;
        
        BatchCreateChildContact blc = new BatchCreateChildContact();        
        Database.executeBatch(blc);
        Test.stopTest();       
           
    }

    static testmethod void test2() {      
        Test.startTest();        

        String parentRecordType = RecordTypeUtility.contactParentVNRT;
        String childRecordType = RecordTypeUtility.contactChildVNRT;
        
		// Parent contact without child
		Datetime dueDate = Datetime.newInstance(2017, 5, 15);
        Contact c = new Contact(firstname='firstname', lastname='lastname', email='firstlast@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date(), Created_From_Due_Date__c = false);       
        insert c;   
        
        // Parent contact with child
        Contact parentContact = new Contact(firstname='firstname', lastname='lastname', email='parent-email@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());        
        insert parentContact;
        
        Datetime childBirthDate = dueDate.addMonths(-10);
        Contact childContact = new Contact(firstname='New - ', lastname='Child', recordtypeid=childRecordType, Birthdate = childBirthDate.date(), AccountId = parentContact.AccountId, Created_From_Due_Date__c = false);        
      //  insert childContact;
        
        BatchCreateChildContact blc = new BatchCreateChildContact();        
        Database.executeBatch(blc);
        Test.stopTest();       
           
    }
    
    
    
}
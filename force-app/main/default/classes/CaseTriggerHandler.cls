/*
 * Name : CaseTriggerHandler
 * Created By : Sakshi Arora
 * Description: Trigger Handler for LeadTrigger
 * Created Date: [11/02/2018]
 * Task : T-747225
 * 
 * */
public class CaseTriggerHandler implements ITriggerHandler {
    
    
    public Boolean isDisabled(){
        
        /*
         *New version using Custom Metadata Types 
         */
        TriggerStatus__mdt triggerStatus = [SELECT Disabled__c FROM TriggerStatus__mdt WHERE label = 'CaseTriggerHandler'][0];
        return triggerstatus.Disabled__c; 
        
        
    }
    
    public void beforeInsert(List<SObject> newItems){

    }
    
    public void beforeUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
        CaseOwnerManager.reassignToManager((List<Case>) newItems.values(), (Map<Id,Case>) oldItems);
        
    }
    
    public void beforeDelete(Map<Id,SObject> oldItems){
        
    }
    
    public void afterInsert(Map<Id,SObject> newItems){
        
                
        
    }
    
    public void afterUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
       
    }
    
    public void afterDelete(Map<Id,SObject> oldItems){
        
    }
    
    public void afterUndelete(Map<Id,SObject> oldItems){
        
    }
    
}
public class ProductLead {

    private Map<String, ID> codeToProductMap = new Map<String, ID>();  
    
    // Populate the Product lookup fields on Lead based on Product Codes
    public void populateProductOnLead (List<Lead> leadList, Map<Id,Lead> oldLeadMap){
        
        // Save the Product_Requested_Code__c in a Set
        for (Lead lead : leadList){
            
            // Check if ProductRequested Code is new or changed and
            // if the Fields are not Null and
            // if the ProductCode is already in the Map before inserting the key
            if( SobjectUtility.isNewOrChanged(lead, 'Product_Requested_Code__c', oldleadMap) &&
               String.isNotBlank(lead.Product_Requested_Code__c) && 
               !codeToProductMap.containsKey(lead.Product_Requested_Code__c) ) {
                   codeToProductMap.put(lead.Product_Requested_Code__c, null);
               }
            
            if( SobjectUtility.isNewOrChanged(lead, 'Current_Product_Code__c', oldleadMap) &&
               String.isNotBlank(lead.Current_Product_Code__c) &&
               !codeToProductMap.containsKey(lead.Current_Product_Code__c) ) {                    
                   codeToProductMap.put(lead.Current_Product_Code__c, null);
               }
        }
        
        
        
        // Query the Products Ids based on Product Codes and save on a Map
        for (Product2 product : [SELECT Id, ProductCode FROM Product2 
                                 WHERE ProductCode IN :codeToProductMap.keySet()] ){
                                     codetoProductMap.put(product.ProductCode, product.Id);
                                 }
        
        
        
        // Populate the Product Fields on Leads Object according to the Products Code
        for (Lead lead : leadList){
            
            if(SobjectUtility.isNewOrChanged(lead, 'Product_Requested_Code__c', oldleadMap) &&
               String.isNotBlank(lead.Product_Requested_Code__c) &&
               codeToProductMap.containsKey(lead.Product_Requested_Code__c) ) {
                   
                   lead.Product_Requested__c = codeToProductMap.get(lead.Product_Requested_Code__c);
                   
               }
            
            if(SobjectUtility.isNewOrChanged(lead, 'Current_Product_Code__c', oldleadMap) &&
               String.isNotBlank(lead.Current_Product_Code__c) &&
               codeToProductMap.containsKey(lead.Current_Product_Code__c) ) {
                   
                   lead.Current_Product_Used__c = codeToProductMap.get(lead.Current_Product_Code__c);
                   
               }
        }
        
    }
    
}
/*
 * Name : ITriggerHandler
 * Created By : Sakshi Arora
 * Description : Interface Trigger Handler 
 * Purpose: To dictates which methods trigger handler must implement
 * Created Date: [10/31/2018]
 * Task : T-746253
 * 
 * */
public interface ITriggerHandler{

  void beforeInsert(List<SObject> newItems);

  void beforeUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems);

  void beforeDelete(Map<Id,SObject> oldItems);

  void afterInsert(Map<Id,SObject> newItems);

  void afterUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems);

  void afterDelete(Map<Id,SObject> oldItems);

  void afterUndelete(Map<Id,SObject> oldItems);

  Boolean isDisabled();
    
}
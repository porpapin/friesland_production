global class createChildFromDueDateSg implements Database.Batchable<sObject>,Database.Stateful {
    global Database.querylocator start(Database.BatchableContext BC){
        //    Date today = Date.today();
        String temp = String.valueOf(system.today());
        String query = 'SELECT ';
        query += ' Id,LastName,AccountId,Address1__c,Due_Date__c,Created_From_Due_Date__c ';
        query += ' ,BirthdateNotVerified__c,Campaign__c,Address2__c,PostalCode__c ';
        query += ' ,CurrentMilkBrand__c,LeadSource,PhoneOptIn__c,EmailOptIn__c,MailOptIn__c,HospitalClinic__c';
        query += ' ,ActivityType__c,LastSRTime__c,Country__c';
        query += ' FROM Contact ';
        query += ' WHERE ';
        query += ' Due_Date__c ='+temp;
        query += ' AND RecordType.Name = \'Parent SG\' ';
        return Database.getQueryLocator(query);
        
    }
    global void execute(Database.BatchableContext BC, List<sObject> scope){    
        List<Contact> contactList = (List<Contact>)scope ;
        List<Contact> childContantCreated = new List<Contact>();
        List<Contact> updateDuedateParent = new List<Contact>();
        Contact childCon;
        for(contact c : contactList){
            childCon = new Contact();
            childCon.RecordTypeId = RecordTypeUtility.contactChildSGRT;
            childCon.AccountId = c.AccountId;
            childCon.FirstName = c.LastName;
            childCon.LastName = 'baby';
            childCon.Campaign__c = c.Campaign__c;
            childCon.Birthdate = c.Due_Date__c;
            childCon.Address1__c = c.Address1__c;
            childCon.Address2__c = c.Address2__c;   
            childCon.PostalCode__c = c.PostalCode__c;
            childCon.CurrentMilkBrand__c = c.CurrentMilkBrand__c;
            childCon.LeadSource = c.LeadSource;
            childCon.PhoneOptIn__c = c.PhoneOptIn__c;
            childCon.MailOptIn__c = c.MailOptIn__c;
            childCon.HospitalClinic__c = c.HospitalClinic__c;
            childCon.Country__c = c.Country__c;
            childCon.ActivityType__c = c.ActivityType__c;
            childCon.ActivityType__c = c.ActivityType__c;
            childCon.BirthdateNotVerified__c = true;
            childCon.Created_From_Due_Date__c = true;
            childContantCreated.add(childCon);
            updateDuedateParent.add(c);
        }
        
        for(Contact c: updateDuedateParent){
            c.Due_Date__c = null;
        }
        
        try{
            if(!childContantCreated.isempty()){
                system.debug('create child contact');
                system.debug('childContantCreated: '+childContantCreated);
                insert childContantCreated;
            }
            
            if(!updateDuedateParent.isempty()){
                system.debug('update due date parent contact');
                system.debug('updateDuedateParent: '+updateDuedateParent);
                update updateDuedateParent;
            }
        }catch(Exception e){
            system.debug('error is: '+e.getMessage());
            system.debug('line is: '+e.getLineNumber());
        }
        
    }
    
    global void finish(Database.BatchableContext BC){              
        system.debug('Batch Finish');
    }      
    
}
/* Created Date: 9 Sept 2019
 * Update Lead Source in existing order
 * Created by : pw
 */ 

global class UpdateAllExistingOrder implements Database.Batchable<sObject> {
    
    
    global UpdateAllExistingOrder(){
    }
    //query From Order
    global Database.QueryLocator start(Database.BatchableContext bc){
        String query = 'SELECT Id, Lead_Source__c,LeadId__c ';
        query += 'FROM Order ';
        return Database.getQueryLocator(query);
    }
    //update ExistingOrder
    global void execute(Database.BatchableContext bc,List<sObject> scope){
        List<Order> orders = (List<Order>)scope;
        Set<Id> leadIds = new Set<Id>();
        for(Order ord : orders){
            leadIds.add(ord.LeadId__c);
        }
    //    system.debug('sdfsdfdsfdsfdsfdsfdsfsdfsfdsfdsfds');
        Map<Id, Lead> mapLeads = new Map<Id, Lead>([
            SELECT Id, LeadSource
            FROM Lead
            WHERE Id IN :leadIds
        ]);

        List<Order> updatedOrder = new List<Order>();
        for(Order ord : orders){
            Lead l = mapLeads.get(ord.LeadId__c);
            if(l != null){
                ord.Lead_Source__c = l.LeadSource;
                    updatedOrder.add(ord);
            }
        }

        if(updatedOrder.size() > 0){
            
           update updatedOrder;
        }
        system.debug('hello');
    }
    
    global void finish(Database.BatchableContext bc){
        
    }
    
    
}
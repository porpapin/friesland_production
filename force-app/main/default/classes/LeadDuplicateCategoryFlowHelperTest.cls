/*
 * Name : LeadDuplicateCategoryFlowHelperTest
 * Created By : Naresh Kumar
 * Description: Class is used to test the LeadDuplicateCategoryFlowHelper class.
 * Created Date: [11 Nov 2018]
 * Task : T-747947
 * */ 
@isTest
public class LeadDuplicateCategoryFlowHelperTest {
    
    @testsetup static void setup(){
        List<Account> testAccts = new List<Account>();
        for(Integer i=0;i<2;i++) {
            testAccts.add(new Account(Name = 'TestAcct'+i));
        }
        insert testAccts;
        Id childRecordTypeId = RecordTypeUtility.contactChildVNRT;
        Id parentRecordTypeId = RecordTypeUtility.contactParentVNRT;
        
        List<Contact> lstContacts = new List<Contact>();
        Contact c=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'email@gmail.com',
            Phone = '9743800309'); 
        c.accountid = testAccts[1].id;
        //This should not pick, because of diffrent account
        //c.Birthdate = Date.newInstance(2018, 11, 12);
        c.Birthdate = system.today();
        c.RecordTypeId = childRecordTypeId;
        lstContacts.add(c); 
        
        c=new Contact(
            FirstName='fname1',
            LastName = 'lname2',
            Email = 'email123@gmail123.com',
            Phone = '9243800308'); 
        c.accountid = testAccts[0].id;
        //This should not pick, because of diffrent recordtype
        //c.Birthdate = Date.newInstance(2018, 11, 12);
        c.Birthdate = system.today();
        c.RecordTypeId = childRecordTypeId;
        lstContacts.add(c); 
        
        c=new Contact(
            FirstName='fnametest',
            LastName = 'lnametest',
            Email = 'emai123l@gmail2.com',
            Phone = '9743800302'); 
        c.accountid = testAccts[0].id;
        c.RecordTypeId = childRecordTypeId;
        c.Birthdate = system.today();
        lstContacts.add(c); 
        insert lstContacts; 
    }
    
    @isTest
    static void testClosestDate()
    {
        Account acct = [SELECT Id FROM Account WHERE Name='TestAcct0' LIMIT 1];
        Test.startTest();
        List<Date> dates = LeadDuplicateCategorizationFlowHelper.getClosestDate(new List<ID>{acct.id});
        Test.stopTest();
        //system.assertEquals( Date.newInstance(2018, 12, 12).addMonths(8), dates[0]);
        system.assertEquals( System.today().addMonths(8), dates[0]);
    }
}
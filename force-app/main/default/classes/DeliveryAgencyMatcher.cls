public class DeliveryAgencyMatcher {
    
    private Set<String> districts;
    private Set<String> provinces;
    private List<String> wards;
    Map<Id,Contact> contactsFromOrders;
    Map<Id,Ward__c> wardsById;
    
    public DeliveryAgencyMatcher(){
        
        districts = new Set<String>();
        provinces = new Set<String>();
        wards = new List<String>();
        contactsFromOrders = new Map<Id,Contact>();
        
    }
    
    public  void assignDeliveryAgencyToOrders(List<Order> orders){
        
        for(Order order : orders){
            copyContactAddressToDeliveryAddress(order);
        }
        
        populateSetsForQuery(orders);
        
        List<Delivery_Agency__c> deliveryAgencies = queryDeliveryAgencies();
        
        for(Order order : orders){
            
            for(Delivery_Agency__c agency : deliveryAgencies){
                if(orderDeliveryAddressMatchesDeliveryAgency(order,agency)){
                    order.Delivery_Agency__c = agency.Id;
                    break;
                }
                
            }
            
        }       
    }
    
    /*
* populates the sets in the class to support the query of Delivery_Agency__c records
*/
    private void populateSetsForQuery(List<Order> orders){
        
        List<Id> wardIds = new List<Id>();
        
        for(Order order : orders){
            if(order.Delivery_Ward__c != null){
                wardIds.add(order.Delivery_Ward__c);  
            }
        }
        
        wardsById = new Map<Id,Ward__c>([SELECT Id, Name FROM Ward__c WHERE Id IN :wardIds]);
        
        
        for(Order order : orders){
            
            districts.add(order.Delivery_District__c);
            provinces.add(order.Delivery_Province__c);
            
            System.Debug('Name of delivery ward while populating sets: '+order.Delivery_Ward_Name__c);
            if(order.Delivery_Ward__c != null && wardsById.containsKey(order.Delivery_Ward__c)){
                wards.add(wardsById.get(order.Delivery_Ward__c).Name);
            }
            
            
        }
        
    } 
    
    private Boolean orderDeliveryAddressMatchesDeliveryAgency(Order order, Delivery_Agency__c agency){
        String deliveryWardName;
        
        if(order.Delivery_Ward__c != null){
        	deliveryWardName = wardsById.get(order.Delivery_Ward__c).Name;
        }
        
        if(order.Delivery_District__c == agency.District__c && order.Delivery_Province__c == agency.Province__c && deliveryWardName == agency.Ward__c){
            return true;
        }
        
        
        return false;
        
    }
    
    private List<Delivery_Agency__c> queryDeliveryAgencies(){
        
        
        List<Delivery_Agency__c> deliveryAgencies = [SELECT Id,Name,District__c,Province__c,Ward__c FROM Delivery_Agency__c
                                                     WHERE District__c IN :districts AND Province__c IN :provinces AND Ward__c IN :wards];
        
        
        return deliveryAgencies;
        
    }
    
    private void copyContactAddressToDeliveryAddress(Order order){
        
        // Copy field values if there's a contact
        if(order.Contact__c != null){
            
            order.Delivery_Street__c = order.Street__c;
            
            if(order.Delivery_Ward__c == null && order.Delivery_District__c == null &&
               order.Delivery_Province__c == null){
                   
                   // Set Delivery Address fields equal to Contact Address fields                   
                   order.Delivery_Ward__c = order.Contact_Ward_ID__c;                   
                   order.Delivery_District__c = order.District__c;
                   order.Delivery_Province__c = order.Province__c;
                   
               }
        }
    }
    
    
    
}
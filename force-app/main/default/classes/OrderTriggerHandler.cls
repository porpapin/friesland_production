public class OrderTriggerHandler implements ITriggerHandler{
    
    public boolean isDisabled(){
        
        TriggerStatus__mdt triggerStatus = [SELECT Disabled__c FROM TriggerStatus__mdt WHERE label = 'OrderTriggerHandler'][0];
        return triggerstatus.Disabled__c; 
        
    }
    public void beforeInsert(List<SObject> newItems){
        
        System.debug('before insert ' + JSON.serializePretty(newItems));
        
        List<Order> orders = (List<Order>) newItems;
        
        DeliveryAgencyMatcher deliveryAgencyMatcher = new DeliveryAgencyMatcher();
        deliveryAgencyMatcher.assignDeliveryAgencyToOrders(orders);
        
        

    }
    
    public void beforeUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
        System.debug('before update ' + JSON.serializePretty(newItems));
        
        Map<Id,Order> orderMap = (Map<Id,Order>) newItems;
        List<Order> orderList = convertMapToList(orderMap);
        DeliveryAgencyMatcher deliveryAgencyMatcher = new DeliveryAgencyMatcher();
        deliveryAgencyMatcher.assignDeliveryAgencyToOrders(orderList);
        
             
    }
    
    public void beforeDelete(Map<Id,SObject> oldItems){

    }
    
    public void afterInsert(Map<Id,SObject> newItems){      
        
    }
    
    public void afterUpdate(Map<Id,SObject> newItems,Map<Id,SObject> oldItems){
        
    }
    
    public void afterDelete(Map<Id,SObject> oldItems){

    }
    
    public void afterUndelete(Map<Id,SObject> oldItems){

    }
    
    public List<Order> convertMapToList(Map<Id,Order> orderMap){
        List<Order> oList = new List<Order>();
        for(Order ord : orderMap.values()){
            oList.add(ord);
        }
        return oList;
    }
    
}
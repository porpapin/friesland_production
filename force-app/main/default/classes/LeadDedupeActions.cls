/*
* Name : LeadDedupeActions
* Created By : Kishore
* Created Date: [31/May/2019]
* Task : T-747965
* Description : Lead Duplication Actions
* As discussed with Pablo and as per the client's specified requirements, here we are not bulkifying this code
* since we are using flow in case of insert and it cannot handle more than one Lead record at a time,
* so we are specifically asked to work with only first record of lead which will be updated.
*
* C-00253748 3/18/19 NCarson - Remove hard coding of Order Type
*
* */
public class LeadDedupeActions{
    //Static Boolean leadUpdatedFromClass = false;
    Static Integer cntr = 0;
    Map<String,String> productRequested = new Map<String,String>();
    LeadDedupeActions_MLY LeadDedupeActionsMLY = new LeadDedupeActions_MLY();
    LeadDedupeActions_VN LeadDedupeActionsVN = new LeadDedupeActions_VN();
    LeadDedupeActions_SGP LeadDedupeActionsSGP = new LeadDedupeActions_SGP();
    public void fireActionsForDedupeResult(Map<Id,Lead> newleadMap,Map<Id,Lead> oldleadMap)
    {
        system.debug('newleadMap :'+newleadMap);
        Map<Id,Lead> newMLYLeadMap = new Map<Id,Lead>();
        Map<Id,Lead> newVNLeadMap = new Map<Id,Lead>();
        Map<Id,Lead> newSGPLeadMap = new Map<Id,Lead>();
        if(!newleadMap.isEmpty()){
            SavePoint savePoint = Database.setSavepoint();
            for(Lead newLeadsObj : newleadMap.Values()){
                if(newLeadsObj.RecordTypeId == RecordTypeUtility.LeadMYRT){
                    newMLYLeadMap.put(newLeadsObj.Id,newLeadsObj);
                }
                if(newLeadsObj.RecordTypeId == RecordTypeUtility.LeadVNRT){
                    newVNLeadMap.put(newLeadsObj.Id,newLeadsObj);
                }
                if(newLeadsObj.RecordTypeId == RecordTypeUtility.LeadSGRT){
                    newSGPLeadMap.put(newLeadsObj.Id,newLeadsObj);
                } 
            }
            if(!newMLYLeadMap.isEmpty() ){
                LeadDedupeActionsMLY.fireActionsForDedupeResult_MLY(newMLYLeadMap, oldleadMap);
            }
            if(!newVNLeadMap.isEmpty() ){
                LeadDedupeActionsVN.fireActionsForDedupeResult_VN(newVNLeadMap, oldleadMap);
            }
           if(!newSGPLeadMap.isEmpty()){
                LeadDedupeActionsSGP.fireActionsForDedupeResult_SGP(newSGPLeadMap, oldleadMap);
            } 
        }
    
    }
}
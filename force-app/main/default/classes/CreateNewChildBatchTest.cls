/* Created Date: 9 Sept 2019
 * Test Class for Schedule Batch
 */ 


@isTest
public class CreateNewChildBatchTest {
    
        @testSetup 
    static void setup() {
        // Load the test data from the static resource        
		       
    }
    
    static testmethod void test() {      
        Test.startTest();        
		Account acc1 = new Account(Name='LN1');
        Account acc2 = new Account(Name='LN2');
        insert acc1;
        insert acc2;
        
        String parentRecordType = RecordTypeUtility.contactParentVNRT;
        String childRecordType = RecordTypeUtility.contactChildVNRT;
        
		// Parent contact without child
		Datetime dueDate = Datetime.newInstance(2019, 1, 15);
        Contact c = new Contact(firstname='firstname', lastname='lastname', email='firstlast@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());       
        insert c;   
        
        // Parent contact with child
        Contact parentContact = new Contact(firstname='firstname', lastname='lastname', email='parent-email@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());        
        insert parentContact;
        
        Datetime childBirthDate = dueDate.addMonths(-10);
        Contact childContact = new Contact(firstname='New - ', lastname='Child', recordtypeid=childRecordType, Birthdate = childBirthDate.date(), AccountId = parentContact.AccountId);        
        insert childContact;
        
        
        // Parent contact without child
		dueDate = Datetime.now().addDays(-1);
        c = new Contact(AccountId = acc1.Id, firstname='firstname woc', lastname='lastname', email='firstlast1@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());       
        insert c;   
        
        // Parent contact with child
        parentContact = new Contact(AccountId = acc2.Id, firstname='firstname wc', lastname='lastname', email='parent-email1@test.com', recordtypeid=parentRecordType, Due_Date__c = dueDate.date());        
        insert parentContact;
        
        childBirthDate = dueDate.addMonths(-10);
        childContact = new Contact(firstname='New - ', lastname='Child', recordtypeid=childRecordType, Birthdate = childBirthDate.date(), AccountId = parentContact.AccountId);        
        insert childContact;
        
        CreateNewChildBatch blc = new CreateNewChildBatch();        
        Database.executeBatch(blc);
        
        CreateNewChildBatchSchedule scheduler = new CreateNewChildBatchSchedule();
        CreateNewChildBatchSchedule.scheduleMe();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '0 0 2 1/1 * ? *';
        String jobID = System.schedule('Batch Look Up Schedule Test', sch, scheduler);
        Test.stopTest();       
    }   

}
/*
* Name : DueDateToContactBatch
* Created By : Sakshi Arora
* Created Date: [11/16/2018]
* Task : T-751749
* Description : Batch Class for scheduling the change of Due date to new contact's birthdate after a month
* */  
global class DueDateToContactBatch implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC)
    {   
        Date today = Date.today();
        System.debug('start batch');
        String query = 'SELECT Id, FirstName,LastName,AccountId,Street__c,Province__c,District__c,Ward__c,Region__c,Due_Date__c '+
                  'FROM Contact Where RecordType.DeveloperName =\'Parent\' and Due_Date__c =:today ';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Contact> scope)
    { 
        List<Contact> conList = new List<Contact>();
        //conlist=[Select id,Name from Contact];
        System.debug('execute batch');
        system.debug('scope' + scope);
        for(Contact c : scope)
        { 
            system.debug('Contact' + c);
            If(c.Due_Date__c == System.today()){
                Contact newContact = new Contact();
                newContact=createChildContact(c);
                conList.add(newContact);
                c.Due_Date__c = null ;
            }          
        }
        
        Database.SaveResult[] srList = Database.insert(conList);
        //system.debug('Passed & Failed' + srList);
        //insert conList;
        //update scope;
        Database.SaveResult[] conUpdate =Database.update(scope);
    }  
    global void finish(Database.BatchableContext BC)
    {
        System.debug('end batch');
    } 
    
    public Contact createChildContact(Contact con){
        Contact contact1 = new Contact();
        Date dueDate = con.Due_Date__c;
        contact1.FirstName = 'New -';
        contact1.LastName = 'Child';
        //contact1.RecordTypeId = '';
        //Siraj# Referencing Recordtypeutility class.
        contact1.RecordTypeId = RecordTypeUtility.contactChildVNRT;
        //contact1.Birthdate = dueDate.addMonths(1);//
        //Right process should be Mom’s due date = Child DOB Dated Wed 15-05-2019 12:17
        contact1.Birthdate = dueDate; 
        contact1.AccountId = con.AccountId;
        //Siraj# Adding Address fields
        contact1.Street__c = con.Street__c;
        contact1.Province__c=con.Province__c;
        contact1.District__c=con.District__c;
        contact1.Ward__c=con.Ward__c;
        contact1.Region__c=con.Region__c;
        return contact1; 
             
    }
}
/* Created date: 9 Sept 2019
 * Schedule to call CreateNewChildBatch
 */

global class CreateNewChildBatchSchedule implements Schedulable{
    
    public static String sched = '0 00 1 * * ?';  //Every Day at 1AM 

    global static String scheduleMe() {
        CreateNewChildBatchSchedule SC = new CreateNewChildBatchSchedule(); 
        return System.schedule('CreateNewChildBatchSchedule', sched, SC);
    }

    global void execute(SchedulableContext sc) {
        System.debug('start batch');
        CreateNewChildBatch b1 = new CreateNewChildBatch();
        ID batchprocessid = Database.executeBatch(b1,20);           
    }
}
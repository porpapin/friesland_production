/*
* Name : LeadDedupeActions_Test
* Created By : Sakshi Arora
* Created Date: [11/08/2018]
* Task : T-747965
* Description : Test class for LeadDedupeActions
*
* C-00253748 3/18/19 NCarson - remove hard coding of order type
*
* */  
@IsTest
public class LeadDedupeActions_Test{
    
    //Test Method to check child record creation using the lead and parent account
    public static testMethod void testLeadConversionWhenBirthDateisPopulated(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentVNRT = RecordTypeUtility.contactParentVNRT;
            
            Account acc = SobjectUtility.createAccount();
            //Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentVNRT,acc.id,Date.today(),null,'Vietnam');
            Product2 prod = SobjectUtility.createProduct();
            
            
            Lead lead = SobjectUtility.createLead('test@test.com','0701233333',Date.today(),null,'Vietnam','test','test');
            lead.Duplicate_Result__c = 'Qualified - New Lead';
            lead.NameOfChild__c = 'test';
            lead.LastNameOfChild__c = 'test';
            update lead;
            Test.stopTest();
            
            List<Lead> leadList = [select id,Existing_Contact__r.id from Lead where id=:lead.id];
            system.assert(leadList.size()>0);
            //system.assertEquals(leadList[0].Existing_Contact__r.id ,matchContact.id);
        }
    }
    
    //Test Method to check child record creation and Lead Creation when DueDate is Populated on Lead
    public static testMethod void testLeadConversionWhenDueDateisPopulated(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentVNRT = RecordTypeUtility.contactParentVNRT;
            
            Account acc = SobjectUtility.createAccount();
            Contact matchContact = SobjectUtility.createContact('test123@test.com','(081) 987-6524',contactParentVNRT,acc.id,null,null,'Vietnam');
            matchContact.Due_Date__c = Date.today().addDays(1);
            update matchContact;
            
            Lead lead = SobjectUtility.createLead('test@test.com','0709799559',null,Date.today().addDays(10),'Vietnam','test','test');
            lead.Duplicate_Result__c = 'Qualified - New Lead';
            update lead;
            Test.stopTest();
            
            List<Order> odrList = [SELECT id,Contact__c,Type FROM Order];
            //system.assert(odrList.size()>0);
            // system.assertEquals( 'Sample',odrList[0].Type); C-00253748 3/18/19 NCarson - remove hard coding of order type
            
        }
    }
    
    //Test Method to Order Product Creation When Lead's Duplicate Result is Existing parent,New child
    public static testMethod void test_OrderProductCreationForExistingparentNewchild(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentVNRT = RecordTypeUtility.contactParentVNRT;
            
            Account acc = SobjectUtility.createAccount();
            
            Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentVNRT,acc.id,null,null,'Vietnam');
            update matchContact;
            
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProduct();
            Lead lead = SobjectUtility.createLead('test@test.com','0709799559',Date.today().addDays(10),null,'Vietnam','test','test');
            lead.Product_Requested__c = prod.Id;
            lead.Duplicate_Result__c = 'Existing parent, new child';
            
            update lead;
            Test.stopTest();
            
            List<PricebookEntry> pbe2 = [Select Id, UnitPrice, Name From PricebookEntry where Product2Id =: prod.Id];
            // system.assert(pbe2.size()>0);
            // system.assertNotEquals(null ,pbe2[0].id);
        } 
    }
    
    //Test Method to check OrderProduct gets populated on Order 
    public static testMethod void test_OrderProductCreationWithExistingPriceBookEntry(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentVNRT = RecordTypeUtility.contactParentVNRT;
            
            Account acc = SobjectUtility.createAccount();
            Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentVNRT,acc.id,null,null,'Vietnam');
            update matchContact;
            
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProduct();
            PricebookEntry pbe1 = SobjectUtility.createPricebookEntry(prod);
            Lead lead = SobjectUtility.createLead('test@test.com','0709799559',null,Date.today().addDays(10),'Vietnam','test','test');
            lead.Product_Requested__c = prod.Id;
            lead.Duplicate_Result__c = 'Existing parent, new child';
            update lead;
            Test.stopTest();
            
            List<PricebookEntry> pbe2 = [Select Id, UnitPrice, Name From PricebookEntry where Product2Id =: prod.Id];
            system.assert(pbe2.size()>0);
            system.assertNotEquals(null ,pbe2[0].id);
        }
    }
    
    //Test Method to update Order 
    public static testMethod void test_OrderUpdate(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForVietnam();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentVNRT = RecordTypeUtility.contactParentVNRT;
            
            Account acc = SobjectUtility.createAccount();
            Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentVNRT,acc.id,null,null,'Vietnam');
            Order myOrder = SobjectUtility.createOrder(acc.Id,matchContact.Id,'Sample','Draft');
            
            
            //order.Delivery_Street__c = order.Street__c;
            myOrder.status='Draft';
            update myOrder;
            
            Test.stopTest();
            
            List<Order> odrList = [SELECT id,Street__c,Delivery_Street__c FROM Order];
            System.assert(odrList.size()>0);
            //System.assertEquals( odrList[0].Street__c, odrList[0].Delivery_Street__c);
            //System.assertEquals( matchContact.Street__c, odrList[0].Delivery_Street__c);
        }
    }
    
    //Test Method to Malaysia Region
    public static testMethod void test_MalaysiaRegion(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForMalaysia();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
            
            Account acc = new Account(Name='Test accountName');
            acc.RecordTypeId = RecordTypeUtility.accountMYRT;
            insert acc;
            
            //Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentMYRT,acc.id,null,null,'Malaysia');
            //update matchContact; 
            
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            Lead lead = SobjectUtility.createLeadMYandSG('test1231@test.com','01122233344',null,Date.today().addYears(-2),thisUser.Country__c,'Lead_MY',false);
            lead.FirstName = 'Test';
            lead.LastName = 'accountName';
            lead.child_dob__c = system.today().addYears(4);
            lead.LastNameOfChild__c = 'TEst1asdasd23';
            lead.Company = 'Malaysia';
            lead.ActivityType__c = 'Sample';
            lead.Product__c = prod.id;
            lead.Existing_Contact__c = null;
            insert lead;
            
            Test.stopTest(); 
            
        }
    }
    
    //Test Method to Malaysia Region
    public static testMethod void test_MalaysiaRegionNewsLetter(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForMalaysia();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
            Id contactChildMYRT = RecordTypeUtility.contactChildMYRT;
            
            Account acc = new Account(Name='Test accountName');
            acc.RecordTypeId = RecordTypeUtility.accountMYRT;
            insert acc;
            
            Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentMYRT,acc.id,null,null,'Malaysia');
            update matchContact; 
            
            Contact matchChildContact = SobjectUtility.createContact('test123@test13.com','(081) 987-4444',contactChildMYRT,acc.id,null,null,'Malaysia');
            matchChildContact.lastName = 'TEst1asdasd23';
            matchChildContact.Birthdate = system.today().addYears(-4);
            update matchChildContact;
            
            
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            
            List<Lead> listLeadsToInsert = new List<Lead>();
            Lead lead = SobjectUtility.createLeadMYandSG('test12231@test.com','01122233344',null,Date.today().addYears(-2),thisUser.Country__c,'Lead_MY',false);
            lead.FirstName = 'Test';
            lead.LastName = 'accountName';
            lead.child_dob__c = system.today().addYears(-4);
            lead.LastNameOfChild__c = 'TEst1asdasd23';
            lead.Company = 'Malaysia';
            lead.LeadSource = 'Hotline';
            lead.Child_Due_Date__c = system.today();
            listLeadsToInsert.add(lead);
            
            Lead leadNew = SobjectUtility.createLeadMYandSG('test12311@test.com','01122233344',null,Date.today().addYears(-2),thisUser.Country__c,'Lead_MY',false);
            leadNew.FirstName = 'Test';
            leadNew.LastName = 'accountName';
            leadNew.child_dob__c = system.today().addYears(-4);
            leadNew.LastNameOfChild__c = 'TEst1asd';
            leadNew.Company = 'Malaysia';
            leadNew.LeadSource = 'Hotline';
            leadNew.ActivityType__c = 'Newsletter';
            leadNew.Duplicate_Result__c = '';
            listLeadsToInsert.add(leadNew);
            insert leadNew;
            
            Test.stopTest(); 
            
        }
    }
    
    //Test Method to check OrderProduct gets populated on Order 
    public static testMethod void test_OrderProductCreationWithExistingPriceBookEntryMalaysia(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForMalaysia();
        
        System.runAs ( thisUser ) {
            Test.startTest();
            Id contactParentMYRT = RecordTypeUtility.contactParentMYRT;
            
            Account acc = new Account(Name='Test accountName');
            acc.RecordTypeId = RecordTypeUtility.accountMYRT;
            insert acc;
            
            Contact matchContact = SobjectUtility.createContact('test@test.com','(081) 987-6524',contactParentMYRT,acc.id,null,null,'Malaysia');
            matchContact.Birthdate = system.today().addYears(-2);
            update matchContact;
            
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            PricebookEntry pbe1 = SobjectUtility.createPricebookEntry(prod);
            Lead lead = SobjectUtility.createLeadMYandSG('test09@test.com','0100979955',Date.today().addDays(10),null,'Malaysia','Lead_MY',false);
            lead.Product_Requested__c = prod.Id;
            lead.Product__c = prod.Id;
            //lead.Duplicate_Result__c = 'Existing parent, existing child';
            lead.Existing_Contact__c = matchContact.id;
            insert lead;
            lead.NameOfChild__c = 'fname';
            lead.LastNameOfChild__c = 'lname';
            lead.Duplicate_Result__c = 'Existing parent, existing child';
            lead.Existing_Contact__c = matchContact.id;
            update lead;
            lead.Duplicate_Result__c = 'Existing parent, new pregnancy';
            update lead;
            Test.stopTest();
            
            List<PricebookEntry> pbe2 = [Select Id, UnitPrice, Name From PricebookEntry where Product2Id =: prod.Id];
            system.assert(pbe2.size()>0);
            system.assertNotEquals(null ,pbe2[0].id);
        }
    }
    
    //Test Method to Singapore Region
    public static testMethod void test_SingaporeRegion(){
        
        //Navin T-794896: Created portal user for test class coverage
        User thisUser = SobjectUtility.portalUserForSingapore();
        
        System.runAs ( thisUser ) {
            
            Test.startTest();
            Id priceBook = Test.getStandardPricebookId();
            Product2 prod = SobjectUtility.createProductMYandSG(thisUser.Country__c);
            Lead lead = SobjectUtility.createLeadMYandSG('test12@test.com','91122233',Date.today().addYears(-2),null,thisUser.Country__c,'Lead_SG',false);
            lead.FirstName = 'Test';
            lead.LastName = 'Account';
            lead.LastNameOfChild__c = 'Tesdasd';
            lead.Product_Requested__c = prod.Id;
            lead.Product__c = prod.Id;
            lead.Company = 'Singapore';
            lead.LeadSource = 'Medical';
            lead.IsLegacy__c  = true;
            insert lead;
            
            Test.stopTest(); 
            
        }
    }
    
    
}
public class RecordTypeUtility {
    
    public static Id accountMYRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_MY').getRecordTypeId();
   // public static Id accountINDRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_IND').getRecordTypeId();
    public static Id accountSGRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_SG').getRecordTypeId();
    public static Id accountVNRT = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Account_VN').getRecordTypeId();
    public static Id contactChildMYRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_MY').getRecordTypeId();
    //public static Id contactChildINDRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_IND').getRecordTypeId();
    public static Id contactChildSGRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_SG').getRecordTypeId();
    public static Id contactChildVNRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child').getRecordTypeId();
    public static Id contactParentMYRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent_MY').getRecordTypeId();
    //public static Id contactParentINDRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent_IND').getRecordTypeId();
    public static Id contactParentSGRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent_SG').getRecordTypeId();
    public static Id contactParentVNRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent').getRecordTypeId();
    public static Id orderMYRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_MY').getRecordTypeId();
    //public static Id orderINDRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_IND').getRecordTypeId();
    public static Id orderSGRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_SG').getRecordTypeId();
    public static Id orderVNRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_VN').getRecordTypeId();
    public static Id LeadMYRT = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
    //public static Id LeadINDRT = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_IND').getRecordTypeId();
    public static Id LeadSGRT = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_SG').getRecordTypeId();
    public static Id LeadVNRT = Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_VN').getRecordTypeId();
    public static Id brandUserRegistrationMYRT = Schema.SObjectType.Brand_User_Registration__c.getRecordTypeInfosByDeveloperName().get('Registration_MY').getRecordTypeId();
    public static Boolean checker = false;
    
    
}
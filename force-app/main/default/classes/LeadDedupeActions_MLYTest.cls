@isTest
public class LeadDedupeActions_MLYTest {
    public static testMethod void testMLYLeadNewLeadSampleFirstLeadWithDOBLT10(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.child_dob__c = system.today().addMonths(-7);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadNewLeadSampleFirstLeadWithDOBMT83(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.child_dob__c = system.today().addMonths(-85);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadNewLeadSampleFirstLeadWithDOB(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadNewLeadSampleFirstLeadWithOutDOB(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.IsFriso__c =  true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadLeadSampleExistingParent(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.IsDL__c= true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.Company = 'Malaysia';
            l1.Product__c = prod.id;
            l1.ActivityType__c='Sample';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            //l.Product_Requested__c = createProduct().Id;
            l1.IsFriso__c = true;
            insert l1;
            
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadLeadSampleExistingParentNewOrder(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.IsDL__c= true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Contact con =[Select Id from Contact where (Email=:l.email or MobilePhone=:l.MobilePhone ) ];
            Order ord =[Select EffectiveDate from Order where Contact__c=:con.Id];
            ord.EffectiveDate = system.today().addMonths(-2);
            update ord;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.Company = 'Malaysia';
            l1.Product__c = prod.id;
            l1.ActivityType__c='Sample';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            //l.Product_Requested__c = createProduct().Id;
            l1.IsFriso__c = true;
            insert l1;
            
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadLeadSampleExistingChilld(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            l1.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l1.Company = 'Malaysia';
            l1.Product__c = prod.id;
            l1.ActivityType__c='Sample';
            l1.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l1;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadLeadSampleExistingChilldNewOrder(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Contact con =[Select Id from Contact where Email=:l.email or MobilePhone=:l.MobilePhone ];
            system.debug('ContactId > '+con.Id);
            Order ord =[Select EffectiveDate from Order where Contact__c=:con.Id];
            system.debug('OrderId >'+ord.Id);
            system.debug('EffectiveDate > '+ord.EffectiveDate);
            ord.EffectiveDate = system.today().addMonths(-2);
            system.debug('EffectiveDate > '+ord.EffectiveDate);
            update ord;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            l1.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l1.Company = 'Malaysia';
            l1.Product__c = prod.id;
            l1.ActivityType__c='Sample';
            l1.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l1;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadSampleLeadNewChilld(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.Product__c = prod.id;
            l.ActivityType__c='Sample';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            l1.child_dob__c = system.today().addYears(-1);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l1.Company = 'Malaysia';
            l1.Product__c = prod.id;
            l1.ActivityType__c='Sample';
            l1.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l1;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadNewLeadPurchaseFirstLeadWithDOB(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            //l.Product__c = prod.id;
            l.ProductPurchase__c = prod.id;
            l.ActivityType__c='Purchase';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadNewLeadPurchaseFirstLeadWithOutDOB(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.ProductPurchase__c = prod.id;
            l.ActivityType__c='Purchase';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.IsFriso__c =  true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadLeadPurchaseExistingParent(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.ProductPurchase__c = prod.id;
            l.ActivityType__c='Purchase';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.IsDL__c= true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            //l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.Company = 'Malaysia';
            l1.ProductPurchase__c = prod.id;
            l1.ActivityType__c='Purchase';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            //l.Product_Requested__c = createProduct().Id;
            l1.IsFriso__c = true;
            insert l1;
            
            Test.stopTest();
        }
        
    }
     
    public static testMethod void testMLYLeadLeadPurchaseExistingChild(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.ProductPurchase__c = prod.id;
            l.ActivityType__c='Purchase';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            l1.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l1.Company = 'Malaysia';
            l1.ProductPurchase__c = prod.id;
            l1.ActivityType__c='Purchase';
            l1.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l1;
            Test.stopTest();
        }
        
    }
    
    public static testMethod void testMLYLeadPurchaseLeadNewChilld(){
        Test.startTest();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            
            Lead l = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l.email = 'email@test.cmo';
            l.MobilePhone = '0104567898';
            l.child_dob__c = system.today().addYears(-2);
            //l.Child_Due_Date__c = dueDate;
            l.Country__c = 'Malaysia';
            l.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l.Company = 'Malaysia';
            Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
            l.ProductPurchase__c = prod.id;
            l.ActivityType__c='Purchase';
            l.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l;
            
            Lead l1 = new Lead(firstname='Test',lastname='11', company='11'); // C-00253748 3/18/19 NCarson - Leads need first names
            l1.email = 'email@test.cmo';
            l1.MobilePhone = '0104567898';
            l1.child_dob__c = system.today().addYears(-1);
            //l.Child_Due_Date__c = dueDate;
            l1.Country__c = 'Malaysia';
            l1.RecordTypeId =Schema.SObjectType.Lead.getRecordTypeInfosByDeveloperName().get('Lead_MY').getRecordTypeId();
            l1.Company = 'Malaysia';
            l1.ProductPurchase__c = prod.id;
            l1.ActivityType__c='Purchase';
            l1.IsDL__c = true;
            //l.Product_Requested__c = createProduct().Id;
            insert l1;
            Test.stopTest();
        }
        
    }
    
    
    
}
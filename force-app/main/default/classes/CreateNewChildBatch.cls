/* Created Date: 9 Sept 2019
* Run to create child at 1AM every day
*/

global class CreateNewChildBatch implements Database.Batchable<sObject>, Database.Stateful {               
    global Database.querylocator start(Database.BatchableContext BC){
        Date today = Date.today().addDays(-1);
        system.debug('Today- 1: ' + today);
        
        String query = 'SELECT ';
        query += ' Id,FirstName,LastName,AccountId,Street__c,Due_Date__c, OwnerId,';
        query += ' Province__c,District__c,Ward__c,Region__c,Created_From_Due_Date__c ';
        query += ' FROM Contact ';
        query += ' WHERE ';
        query += ' Due_Date__c =: today ';
        //    query += ' AND Due_Date__c != null ';
        query += ' AND RecordType.Name = \'Parent VN\'';
        System.debug(query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){    
        try{
            system.debug('try');
            Map<String, Object> result = processData(scope);
            List<Contact> newChildContacts = (List<Contact>)result.get('newChildContacts');
            Map<Id, Account> updatedAccounts = (Map<Id, Account>)result.get('updatedAccounts');
            Map<Id, Contact> updatedParentContacts = (Map<Id, Contact>)result.get('updatedParentContacts');
            
            if(newChildContacts != null && newChildContacts.size() > 0){
                insert newChildContacts;
            }
            if(updatedParentContacts != null && updatedParentContacts.values().size() > 0){
                update updatedParentContacts.values();
            }
            if(updatedAccounts != null && updatedAccounts.values().size() > 0){
                update updatedAccounts.values();
            }
        } catch(Exception e){
            system.debug(e.getLineNumber());
            system.debug(e.getMessage());
        }
    }
    
    global Map<String, Object> processData(List<sObject> scope){
        system.debug('scope ' + scope);
        List<Contact> newChildContacts = new List<Contact>();
        Map<Id, Account> updatedAccounts = new Map<Id, Account>();
        Map<Id, Contact> updatedParentContacts = new Map<Id, Contact>();
        
        List<Contact> parentContacts = (List<Contact>)scope;
        
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Contact> mapAccountParentContact = new Map<Id, Contact>();
        for(Contact c : parentContacts){
            accountIds.add(c.AccountId);
            mapAccountParentContact.put(c.AccountId, c);
        }
        system.debug('mapAccountParent' + mapAccountParentContact);
        //Get Account to update new_duedate__c        
        List<Account> accounts = [
            SELECT 
            Id, Name, new_duedate__c 
            FROM Account 
            WHERE Id IN :accountIds AND new_duedate__c != null
        ];
        system.debug('Accounts' + accounts);
        
        //Get Child Contact Lastest Child
        List<Contact> childsContacts = [
            SELECT Id, AccountId, BirthDate, Created_From_Due_Date__c 
            FROM Contact 
            WHERE 
            RecordType.Name = 'Child VN'
            AND AccountId IN :accountIds 
            AND BirthDate != NULL 
            ORDER BY BirthDate DESC LIMIT 1
        ];
        system.debug('childsContacts' + childsContacts);
        
        Map<Id, List<Contact>> mapAccountChildContact = new Map<Id, List<Contact>>();
        if(childsContacts != null){
            for(Contact c : childsContacts){                
                if(!mapAccountChildContact.containsKey(c.AccountId)){
                    mapAccountChildContact.put(c.AccountId, new List<Contact>());
                }
                mapAccountChildContact.get(c.AccountId).add(c);
            }
        }
        system.debug('mapAccountChildContact' + mapAccountChildContact);
        for(id accountId : accountIds)
        {
            Contact parent = mapAccountParentContact.get(accountId);
            List<Contact> childContact = mapAccountChildContact.get(parent.AccountId);
            
            if(childContact != null && childContact.size() > 0){
                // Parent have child contact
                Boolean createdNewChild = false;
                for(Contact child : childContact){
                    system.debug('have child');
                    if(createdNewChild){
                        break;
                    }
                    if(parent.Due_Date__c <= child.Birthdate.addMonths(8)){
                        //add logic to check <+8 months so clear out account and parent due date
                        
                        system.debug('update account parent only');
                        Contact con = new Contact();
                        con.Id = parent.Id;
                        con.Due_Date__c = null;
                        Account acc = new Account();
                        acc.Id = parent.AccountId;
                        acc.new_duedate__c = null;
                        updatedParentContacts.put(parent.Id, con);
                        
                        // Update account due date
                        updatedAccounts.put(parent.AccountId, acc); 
                    }
                    
                    else if(parent.Due_Date__c > child.Birthdate.addMonths(8))
                    {
                        //parent has existing child
                        String parentContactJSON = JSON.serialize(parent);
                        Contact contactClone = (Contact)JSON.deserialize(parentContactJSON, Contact.class);                                                
                        contactClone.FirstName = 'New -';
                        contactClone.LastName = 'Child';
                        contactClone.RecordTypeId = RecordTypeUtility.contactChildVNRT;
                        contactClone.Birthdate = parent.Due_Date__c;
                        contactClone.Created_From_Due_Date__c = true;
                        // contactClone.AccountId = parent.AccountId;
                        // contactClone.Street__c = parent.Street__c;
                        // contactClone.Province__c=parent.Province__c;
                        // contactClone.District__c=parent.District__c;
                        // contactClone.Ward__c=parent.Ward__c;
                        // contactClone.Region__c=parent.Region__c;
                        contactClone.Due_Date__c = null;
                        contactClone.Id = null;                        
                        newChildContacts.add(contactClone);
                        
                        // Update parent contact due date
                        Contact con = new Contact();
                        con.Id = parent.Id;
                        con.Due_Date__c = null;
                        
                        Account acc = new Account();
                        acc.Id = parent.AccountId;
                        acc.new_duedate__c = null;
                        updatedParentContacts.put(parent.Id, con);
                        
                        // Update account due date
                        
                        updatedAccounts.put(parent.AccountId, acc); 
                        createdNewChild = true;
                    }
                }
                
            } else {
                // Parent don't have any child contact
                if (parent.Due_Date__c != NULL)
                {
                    //parent has no child
                    System.debug('no child');
                    String parentContactJSON = JSON.serialize(parent);
                    Contact contactClone = (Contact)JSON.deserialize(parentContactJSON, Contact.class);                    
                    contactClone.FirstName = 'New -';
                    contactClone.LastName = 'Child';
                    contactClone.RecordTypeId = RecordTypeUtility.contactChildVNRT;
                    contactClone.Birthdate = parent.Due_Date__c;
                    contactClone.Created_From_Due_Date__c = true;
                    // contactClone.AccountId = parent.AccountId;
                    // contactClone.Street__c = parent.Street__c;
                    // contactClone.Province__c=parent.Province__c;
                    // contactClone.District__c=parent.District__c;
                    // contactClone.Ward__c=parent.Ward__c;
                    // contactClone.Region__c=parent.Region__c;                 
                    contactClone.Due_Date__c = null;
                    contactClone.Id = null;
                    
                    newChildContacts.add(contactClone);
                    
                    // Update parent contact due date
                    Contact con = new Contact();
                    con.Id = parent.Id;
                    con.Due_Date__c = null;
                    
                    Account acc = new Account();
                    acc.Id = parent.AccountId;
                    acc.new_duedate__c = null;
                    updatedParentContacts.put(parent.Id, con);
                    
                    // Update account due date
                    
                    updatedAccounts.put(parent.AccountId, acc); 
                    system.debug(updatedAccounts);
                }
            }  
        } 
        Map<String, Object> result = new Map<String, Object>();
        result.put('newChildContacts', newChildContacts);
        result.put('updatedAccounts', updatedAccounts);
        result.put('updatedParentContacts', updatedParentContacts);
        system.debug('updatedParentContacts' + updatedParentContacts);
        system.debug('updatedAccounts' + updatedAccounts);
        return result;
    }
    
    global void finish(Database.BatchableContext BC){                
    }      
}
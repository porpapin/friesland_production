global class SCSearchController {

    class SCCustomer {
        public String objectType;
        List<Contact> conts;
        List<Lead> leads;
        List<Account> accs;
    }
    
    public String contactName {get; set;}
    public List<Contact> contactList {get; set;}
      
    public SCSearchController() {
        contactName = '';
        doSearch();
    }
    
    public void doSearch() {
        contactList = [SELECT id, phone, firstname, lastname FROM Contact WHERE (firstname LIKE :('%' + contactName + '%') OR lastname LIKE :('%' + contactName + '%')) LIMIT 5];
    }
    
    webService static String getCustomerByPhone(String phone) { 
        SCCustomer scCus =  new SCCustomer();
        scCus.conts = [Select Id, Name, Phone from Contact WHERE Phone =:phone or mobilephone=:phone order by name];
        if (scCus.conts != null && !scCus.conts.isEmpty())
        {
            scCus.objectType = 'Contact';
            return JSON.serialize(scCus);
        }
        
        scCus.leads = [Select Id, Name, Phone from Lead WHERE Phone =:phone or mobilephone=:phone order by name];
        if (scCus.leads != null && !scCus.leads.isEmpty())
        {
            scCus.objectType = 'Lead';
            return JSON.serialize(scCus);
        }
        return '';
    } 
    
    webService static String getContacts(String ani) { 
        List<Contact> contacts = new List<Contact>(); 
        for (Contact contact : [Select Id, Name, Phone from Contact WHERE (firstname LIKE :('%' + ani + '%') OR lastname LIKE :('%' + ani + '%') Or Phone LIKE :('%' + ani + '%')) order by name LIMIT 5]){ 
            contacts.add(contact);
        }
        return JSON.serialize(contacts);
    } 
    
    webService static String getAccountByContact(String contactid) { 
        List<Contact> contacts = new List<Contact>(); 
        for (Contact contact : [Select accountId from Contact WHERE id = :contactid]){ 
            contacts.add(contact);
        }
        return JSON.serialize(contacts);
    } 
    
    webService static String getCasesByContactId(String cusid) { 
        List<Case> cases = new List<Case>();
        for (Case acase : [Select Id, casenumber, contactId, subject, CreatedDate  from Case where contactId = :cusid order by CreatedDate desc LIMIT 5]){
            cases.add(acase);
        }
        return JSON.serialize(cases);
    }
    
    webService static String getCasesByAccountId(String cusid) { 
        List<Case> cases = new List<Case>();
        for (Case acase : [Select Id, casenumber, accountId, subject, CreatedDate  from Case where accountId = :cusid order by CreatedDate desc LIMIT 5]){
            cases.add(acase);
        }
        return JSON.serialize(cases);
    }
    
    webService static String getCases() { 
        List<Case> cases = new List<Case>();
        for (Case acase : [Select Id, casenumber, accountId, contactId, subject, CreatedDate  from Case Where CreatedById = :userInfo.getUserId() order by CreatedDate desc LIMIT 5]){
            cases.add(acase);
        }
        return JSON.serialize(cases);
    }

    webService static String getCallHistoryByContactId(String cusid) { 
        List<Task> tasks = new List<Task>();
        for (Task task : [SELECT  ID, whoid,  CallObject, CallDurationInSeconds, CallType, Description, OwnerId, Subject, Type, ActivityDate, CreatedDate FROM Task where whoid =:cusid and Type = 'Call' ORDER By CreatedDate desc limit 5]){
            tasks.add(task);
        }
        return JSON.serialize(tasks);
    }

    webService static String getCallHistory() { 
        List<Task> tasks = new List<Task>();
        for (Task task : [SELECT  ID, whoid,  CallObject, Status, Customer_Name__c, Phone__c, Connected_Time__c,Disconnected_Time__c, CallDurationInSeconds, CallType, Description, OwnerId, Subject, ActivityDate, CreatedDate FROM Task where CreatedById = :userInfo.getUserId() and Type = 'Call' ORDER By CreatedDate desc limit 50]){
            tasks.add(task);
        }
        return JSON.serialize(tasks);
    }
    webService static String getCallMiss() { 
        List<Task> tasks = new List<Task>();
        for (Task task : [SELECT  ID, whoid,  CallObject, Status, Customer_Name__c, Phone__c, Connected_Time__c,Disconnected_Time__c, CallDurationInSeconds, CallType, Description, OwnerId, Subject, ActivityDate, CreatedDate FROM Task where( CreatedById = :userInfo.getUserId() and CallType='Inbound' and Connected_Time__c = null) and Type = 'Call' ORDER By CreatedDate desc limit 50]){
            tasks.add(task);
        }
        return JSON.serialize(tasks);
    }   
}
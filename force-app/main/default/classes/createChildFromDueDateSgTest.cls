@isTest
public class createChildFromDueDateSgTest {
    static testmethod void test() {
        Test.startTest();        
        String parentRecordType = RecordTypeUtility.contactParentSGRT;
        String accountRecordType = RecordTypeUtility.accountSGRT;
        Account acc = new Account(Name='LN1',RecordtypeId =accountRecordType );
        insert acc;
        Contact c = new Contact(firstname='firstname', lastname='lastname', email='firstlast@test.com', recordtypeid=parentRecordType, Due_Date__c = system.today(),AccountId=acc.Id);       
        insert c;   
        acc.PrimaryContact__c = c.Id;
        update acc;
        
        createChildFromDueDateSg blc = new createChildFromDueDateSg();        
        Database.executeBatch(blc);
        
        createChildFromDueDateSgSchedule scheduler = new createChildFromDueDateSgSchedule();
        createChildFromDueDateSgSchedule.scheduleMe();
        // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
        String sch = '0 0 2 1/1 * ? *';
        String jobID = System.schedule('Batch Look Up Schedule Test', sch, scheduler);
        Test.stopTest();
    }
    
}
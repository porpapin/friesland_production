/*
* Name : LeadDuplicateCategorizationFlowHelper
* Created By : Naresh Kumar
* Description: Class is used to invoke from LeadDuplicateCategorizationFlow. It will find closest date of all the contacts from account id passed from Flow.
* Created Date: [11 Nov 2018]
* Task : T-747947
* 
* */
public class LeadDuplicateCategorizationFlowHelper {
    
    @InvocableMethod
    public static List<Date> getClosestDate(List<Id> accountIdList)
    {
        String AcntRecordTypeDeveloperName;
        String childContactRecordTypeDeveloperName;
        for(Account acnt : [Select id, Name, RecordType.DeveloperName from Account where id =:accountIdList[0] limit 1]){
            AcntRecordTypeDeveloperName = acnt.RecordType.DeveloperName;
        }
        system.debug('AcntRecordTypeDeveloperName--->'+AcntRecordTypeDeveloperName);
        if(AcntRecordTypeDeveloperName.equalsIgnoreCase('Account_VN')){
            childContactRecordTypeDeveloperName = 'Child';
        } else if(AcntRecordTypeDeveloperName.equalsIgnoreCase('Account_MY')){
            childContactRecordTypeDeveloperName = 'Child_MY';
        } else if(AcntRecordTypeDeveloperName.equalsIgnoreCase('Account_SG')){
            childContactRecordTypeDeveloperName = 'Child_SG';
        }
        system.debug('childContactRecordTypeDeveloperName-->'+childContactRecordTypeDeveloperName);
        if(accountIdList!=null && accountIdList.size()>0)
        {
            //Fetch all the child contacts of an account
            //List<contact> contactList = [Select id, birthdate from contact where accountid=:accountIdList[0] 
            //                             And RecordType.Name = 'Child' AND birthdate <> null];
            List<contact> contactList = [Select id, birthdate from contact where accountid=:accountIdList[0] 
                                         And RecordType.DeveloperName =: childContactRecordTypeDeveloperName AND birthdate <> null];
            Date birthdateClosestToNow =null;
            
            for(contact cont :contactList )
            {
                if(birthdateClosestToNow == null )
                {
                    birthdateClosestToNow = cont.Birthdate;
                }else
                {
                    Integer currentDate =Math.abs( Date.today().daysBetween( cont.Birthdate));
                    Integer oldDate =Math.abs( Date.today().daysBetween( birthdateClosestToNow));
                    //compare for least no of days from today 
                    if(currentDate<oldDate)
                    {
                        birthdateClosestToNow = cont.Birthdate;
                    }
                }
            }
            if(birthdateClosestToNow!=null){
                //Add 8 months to the closest date to compare in the flow
                birthdateClosestToNow = birthdateClosestToNow.addMonths(8); 
            }
            system.debug('birthdateClosestToNow----->'+birthdateClosestToNow);
            return new List<Date>{birthdateClosestToNow};
                }else{
                    return null;
                }
        
    }
}
@isTest
public class DeliveryAgencyMatcher_Test {
    
    public static Map<String,Id> wardByDistrict = new Map<String,Id>();
    public static Map<Id,String> districtByWard = new Map<Id,String>();
    public static List<String> deliveryDistricts = new List<String>();
    public static Map<String,String> deliveryProvincesByDistrict = new Map<String,String>();
    public static Map<String,Id> deliveryAgencyByDistrict = new Map<String,Id>();
    public static Account account;
    public static List<Contact> contacts = new List<Contact>();
    
    private static void createRecordDependencies(){
        
        // Create Wards
        List<Ward__c> wards = new List<Ward__c>();
        wards.add(new Ward__c(Name = '1', District__c = 'Kiến Tường'));
        wards.add(new Ward__c(Name = '1', District__c = 'Giá Rai'));
        wards.add(new Ward__c(Name = '1', District__c = 'Bến Tre'));
        wards.add(new Ward__c(Name = '1', District__c = 'Duyên Hải'));
        wards.add(new Ward__c(Name = '1', District__c = 'Cai Lậy'));      
        insert wards;
        
        for(Ward__c ward : wards){
            wardByDistrict.put(ward.District__c,ward.Id);
        }
        
        for(Ward__c ward: wards){
            districtByWard.put(ward.Id,ward.District__c);
        }
        
        // Create Account
        account = new Account();
        account.Name = 'My Account';
        insert account;
        
        // Create Contacts
        
        contacts.add(new Contact(Birthdate = Date.today(), LastName = 'LastName 1', District__c = 'Kiến Tường',
                               Province__c = 'Long An', Ward__c = wards[0].Id, AccountId = account.Id));
        contacts.add(new Contact(Birthdate = Date.today(), LastName = 'LastName 2', District__c = 'Giá Rai',
                               Province__c = 'Bạc Liêu', Ward__c = wards[1].Id, AccountId = account.Id));
        contacts.add(new Contact(Birthdate = Date.today(), LastName = 'LastName 3', District__c = 'Bến Tre',
                               Province__c = 'Bến Tre', Ward__c = wards[2].Id, AccountId = account.Id));
        contacts.add(new Contact(Birthdate = Date.today(), LastName = 'LastName 4', District__c = 'Duyên Hải',
                               Province__c = 'Trà Vinh', Ward__c = wards[3].Id, AccountId = account.Id));
        contacts.add(new Contact(Birthdate = Date.today(), LastName = 'LastName 5', District__c = 'Cai Lậy',
                               Province__c = 'Tiền Giang', Ward__c = wards[4].Id, AccountId = account.Id));
        
        
        insert contacts;
        
        // Create Delivery Agencies
        List<Delivery_Agency__c> deliveryAgencies = new List<Delivery_Agency__c>();
        deliveryAgencies.add(new Delivery_Agency__c(Name = 'Lazada', District__c = 'Kiến Tường',
                                                 Province__c = 'Long An', Ward__c = '1'));
        deliveryAgencies.add(new Delivery_Agency__c(Name = 'Lazada', District__c = 'Giá Rai',
                                                 Province__c = 'Bạc Liêu', Ward__c = '1'));
        deliveryAgencies.add(new Delivery_Agency__c(Name = 'Lazada', District__c = 'Bến Tre',
                                                 Province__c = 'Bến Tre', Ward__c = '1'));
        deliveryAgencies.add(new Delivery_Agency__c(Name = 'Lazada', District__c = 'Duyên Hải',
                                                 Province__c = 'Trà Vinh', Ward__c = '1'));
        deliveryAgencies.add(new Delivery_Agency__c(Name = 'Lazada', District__c = 'Cai Lậy',
                                                 Province__c = 'Tiền Giang', Ward__c = '1'));
             
        
        insert deliveryAgencies;
               
        for(Delivery_Agency__c agency : deliveryAgencies){
            deliveryAgencyByDistrict.put(agency.District__c, agency.Id);
        }
        
        deliveryDistricts.add('Cai Lậy');
        deliveryDistricts.add('Duyên Hải');
        deliveryDistricts.add('Bến Tre');
        deliveryDistricts.add('Giá Rai');
        deliveryDistricts.add('Kiến Tường');
        
        deliveryProvincesByDistrict.put('Cai Lậy','Tiền Giang');          
        deliveryProvincesByDistrict.put('Duyên Hải','Trà Vinh');
        deliveryProvincesByDistrict.put('Bến Tre','Bến Tre');
        deliveryProvincesByDistrict.put('Giá Rai','Bạc Liêu');
        deliveryProvincesByDistrict.put('Kiến Tường','Long An');
        
    }
    
    
    public static testMethod void test_assignDeliveryAgencyToOrders(){
        
        createRecordDependencies();
          
        Test.startTest();
        
        Map<String, String> OrdersMap = new Map<String, String>();
        
        // Create 100 Orders
        List<Order> orders = new List<Order>();
        for(Integer o = 0; o < 5; o++){
           
            // Tests code under conditions of different contact address and delivery address
            orders.add(new Order(Name = 'Test Order ' + o, contact__c = contacts[math.mod(o, contacts.size())].Id,
                                AccountId = account.Id, EffectiveDate = Date.today(), Status = 'Draft',
                                Delivery_District__c = deliveryDistricts[math.mod(o,deliveryDistricts.size())],
                                Delivery_Province__c = deliveryProvincesByDistrict.get(deliveryDistricts[math.mod(o,deliveryDistricts.size())]),
                                Delivery_Ward__c = wardByDistrict.get(deliveryDistricts[math.mod(o,deliveryDistricts.size())])));
                                
            // Tests code under conditions of null delivery address ward, district, and province
            orders.add(new Order(Name = 'Test Order ' + o, contact__c = contacts[math.mod(o, contacts.size())].Id,
                                AccountId = account.Id, EffectiveDate = Date.today(), Status = 'Draft'));
        }
        
       
        insert orders;
        
        Test.stopTest();
        
        List<Order> insertedOrders = [SELECT Id, Delivery_Agency__c ,Delivery_Ward__c FROM Order];
        
        for(Order order : insertedOrders){ 
            
            String deliveryDistrict = districtByWard.get(order.Delivery_Ward__c); 
            
            Id deliveryAgencyForDistrict = deliveryAgencyByDistrict.get(deliveryDistrict); 
            
            Delivery_Agency__c agency = [SELECT Id,District__c,Ward__c,Province__c FROM Delivery_Agency__c WHERE Id = :deliveryAgencyForDistrict LIMIT 1];
            
            System.assert(order.Delivery_Agency__c == deliveryAgencyForDistrict);
        }
        
    }

}
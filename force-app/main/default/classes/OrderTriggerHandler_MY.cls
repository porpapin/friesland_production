/**==============================================================================================================   
* Appirio, Inc
* Name: OrderTriggerHandler 
* @story: I-369238 - I-369242/S-609055
* Created By			Date(D/M/Y)			Description
* Navin Dayama			03/20/2019			Order created on account with child between 10 months and 84 months.
* 											And more than 1 order not allowed to create in same month
* =============================================================================================================*/
public class OrderTriggerHandler_MY {
    static Id rtOrderMalaysia  = RecordTypeUtility.orderMYRT; 
    static Id rtOrderSingapore = RecordTypeUtility.orderSGRT; 
    
    public static void onBeforeInsert(List<Order> triggerNew){
    	
        String frisoGoldProducts = Label.Friso_Gold_Products;
        String frisoMumProducts = Label.Friso_Mum_Products;
        String dutchLadyProducts = Label.Dutch_Lady_Products;
        Set<String> FGProductsSet = new Set<String>();
        Set<String> FMProductsSet = new Set<String>();
        Set<String> DLProductsSet = new Set<String>();
        if(String.isNotBlank(frisoGoldProducts)){
            for(String fgProd : frisoGoldProducts.split(',')){
                FGProductsSet.add(fgProd.trim()); 
            }
        }
        if(String.isNotBlank(frisoMumProducts)){
            for(String fmProd : frisoMumProducts.split(',')){
                FMProductsSet.add(fmProd.trim()); 
            }
        }
        if(String.isNotBlank(dutchLadyProducts)){
            for(String dlProd : dutchLadyProducts.split(',')){
                DLProductsSet.add(dlProd.trim()); 
            }
        }
        
        Set<String> FGProductsIdSet = new Set<String>();
        Set<String> FMProductsIdSet = new Set<String>();
        Set<String> DLProductsIdSet = new Set<String>();
        for(Product2 prod : [SELECT Id 
                             FROM Product2 
                             WHERE Name in : FGProductsSet
                             AND IsActive = true]){
            FGProductsIdSet.add(prod.Id);
        }
        
        for(Product2 prod : [SELECT Id 
                             FROM Product2 
                             WHERE Name in : FMProductsSet
                             AND IsActive = true]){
            FMProductsIdSet.add(prod.Id);
        }
        
        for(Product2 prod : [SELECT Id 
                             FROM Product2 
                             WHERE Name in : DLProductsSet
                             AND IsActive = true]){
            DLProductsIdSet.add(prod.Id);
        }
        
        for(Order ord : triggerNew){
            if((ord.RecordTypeId == rtOrderMalaysia || ord.RecordTypeId == rtOrderSingapore) 
               && ord.Type == 'Sample' && ord.Child_Contact__c != null && ord.Sample__c != null){
                    for(Order existingOrder : [ Select id, Child_Contact__c, Sample__c, Sample__r.Name, EffectiveDate
                                                 From Order
                                                 Where Child_Contact__c =: ord.Child_Contact__c
                                                 And Sample__c != null]){
						if(FGProductsIdSet.contains(existingOrder.Sample__c) &&
                           FGProductsIdSet.contains(ord.Sample__c)&&
                           ord.EffectiveDate <= existingOrder.EffectiveDate.addDays(
                               Integer.valueOf(Label.Same_Month_Number_of_Days_Check)
                           )){
                            ord.addError('Lead not Converted, Same Product Order Already Exist In This Month');                             
						}
						if(FMProductsIdSet.contains(existingOrder.Sample__c) &&
                           FMProductsIdSet.contains(ord.Sample__c)&&
                           ord.EffectiveDate <= existingOrder.EffectiveDate.addDays(
                               Integer.valueOf(Label.Same_Month_Number_of_Days_Check)
                           )){
                            ord.addError('Lead not Converted, Same Product Order Already Exist In This Month');                             
						} 
						if(DLProductsIdSet.contains(existingOrder.Sample__c) &&
                           DLProductsIdSet.contains(ord.Sample__c)&&
                           ord.EffectiveDate <= existingOrder.EffectiveDate.addDays(
                               Integer.valueOf(Label.Same_Month_Number_of_Days_Check)
                           )){
                            ord.addError('Lead not Converted, Same Product Order Already Exist In This Month');                             
						}
                }
            }
        }
    }
}
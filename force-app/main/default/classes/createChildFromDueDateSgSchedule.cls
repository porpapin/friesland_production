global class createChildFromDueDateSgSchedule implements Schedulable{
    
    public static String sched = '0 00 3 * * ?';  //Every Day at 1AM 

    global static String scheduleMe() {
        createChildFromDueDateSgSchedule SC = new createChildFromDueDateSgSchedule(); 
        return System.schedule('Create Child Batch SG', sched, SC);
    }

    global void execute(SchedulableContext sc) {
        System.debug('start batch');
        createChildFromDueDateSg b1 = new createChildFromDueDateSg();
        ID batchprocessid = Database.executeBatch(b1,20);           
    }
}
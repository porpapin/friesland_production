@isTest
private class FC_Test_LeadDedupeActions_1 {
     @isTest static void test_method_one() {
         List<RecordType> ldRecTypes = [SELECT Id From RecordType where Name = 'Lead MY'];
         List<RecordType> ConRecTypesPMy = [SELECT Id From RecordType where Name = 'Parent MY'];
         List<RecordType> ConRecTypesChMy = [SELECT Id From RecordType where Name = 'Child MY'];
         List<RecordType> AccntRecTypesPMy = [SELECT Id From RecordType where Name = 'Account MY'];
         Product2 prod = new Product2(Name='Friso',CurrencyIsoCode = 'MYR',isActive=true);
         insert prod;
         List<Lead> ldsLst = new List<Lead>();
         Lead ld1 = new Lead(LastName='MyTest33',Email='mytest33@test.com',Country__c='Malaysia',child_dob__c=System.today() - 11,
                            MobilePhone='0124630411',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id);
         ldsLst.add(ld1);
         Lead ld2 = new Lead(LastName='MyTest33',Email='mytest343@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2011,12,12),
                            MobilePhone='0124630412',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id);
         ldsLst.add(ld2);
         Lead ld3 = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2016,02,02),
                            MobilePhone='0124630413',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id);
         ldsLst.add(ld3);
         Lead ld4 = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2016,02,02),
                            MobilePhone='0124630413',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id);
         ldsLst.add(ld4);
         
         Lead ldnewLetter = new Lead(LastName='MyTest33',Email='mytest35NewLetter@test.com',Country__c='Malaysia',child_dob__c=System.today() - 40,
                            MobilePhone='0124630913',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id,ActivityType__c='Newsletter');
         ldsLst.add(ldnewLetter);
         if(!ldsLst.isEmpty()){
         	database.insert(ldsLst);    
         }
         Lead ld5 = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2018,02,02),
                            MobilePhone='0124630413',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',ActivityType__c ='Sample',
                            recordtypeId=ldRecTypes[0].Id);
         insert ld5;
         
         Lead ldexist = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2018,02,02),
                            MobilePhone='0124630413',ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',ActivityType__c ='Purchase',
                            recordtypeId=ldRecTypes[0].Id);
         insert ldexist;
         Lead ldexist1 = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',child_dob__c=Date.newInstance(2018,02,02),
                            MobilePhone='0124630413',ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',ActivityType__c ='Purchase',
                            recordtypeId=ldRecTypes[0].Id);
         insert ldexist1;
         
         test.startTest();
         Lead ld6 = new Lead(LastName='MyTest33',Email='mytest34@test.com',Country__c='Malaysia',Child_Due_Date__c=System.today() -170,
                            MobilePhone='0124630414',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id);
         insert ld6;
         
         Lead ld7 = new Lead(LastName='MyTest33New',Email='mytest34@test.com',Country__c='Malaysia',Child_Due_Date__c=System.today() + 160,
                            MobilePhone='0124630414',Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id);
         insert ld7;
         
        
         //existing parent new pregnancy
                 
         Lead ldexistpreg = new Lead(LastName='MyTest33',Email='mytest35@test.com',Country__c='Malaysia',Child_Due_Date__c=System.today() -170,
                            MobilePhone='0124630413',ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',ActivityType__c ='Purchase',
                            recordtypeId=ldRecTypes[0].Id);
         insert ldexistpreg;
         test.stopTest();
     }
    @isTest static void test_method_VN() {
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
          User u = new User(Alias = 'Tests', Email='testsuser@testorg.com', 
          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
          LocaleSidKey='en_US', ProfileId = p.Id, 
          TimeZoneSidKey='America/Los_Angeles', UserName='testsuser@testorg.com',Country='Vietnam');
        System.runAs(u) {
            List<RecordType> ldRecTypes = [SELECT Id From RecordType where Name = 'Lead VN'];
         	List<RecordType> ConRecTypesPMy = [SELECT Id From RecordType where Name = 'Parent VN'];
         	List<RecordType> ConRecTypesChMy = [SELECT Id From RecordType where Name = 'Child VN'];
         	List<RecordType> AccntRecTypesPMy = [SELECT Id From RecordType where Name = 'Account VN'];
         	Product2 prod = new Product2(Name='Friso',CurrencyIsoCode = 'VND',isActive=true);
         	insert prod;
             List<lead> ldlstVt = new list<Lead>(); 
       		 Lead ld11 = new Lead(FirstName='tst',LastName='MyTest',child_dob__c=Date.newInstance(2015,10,01),
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177004',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test122@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        
        	ldlstVt.add(ld11);
        	Lead ld12 = new Lead(FirstName='tst',LastName='MyTest',child_dob__c=Date.newInstance(2015,10,01),
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177002',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1221@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        	ldlstVt.add(ld12);
            
            Lead ld14 = new Lead(FirstName='tst',LastName='MyTest',child_dob__c=Date.newInstance(2015,10,01),
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177002',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1221@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        	ldlstVt.add(ld14);
        	database.insert(ldlstVt);
            Lead ld13 = new Lead(FirstName='tst',LastName='MyTest',Child_Due_Date__c=System.today() - 160,
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177005',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1225@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        	insert ld13;
            
            Lead ld17 = new Lead(FirstName='tst',LastName='MyTest',Child_Due_Date__c=System.today() - 160,
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177005',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1225@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        	insert ld17;
            
            Lead ld16 = new Lead(FirstName='tst',LastName='MyTest',child_dob__c=Date.newInstance(2017,10,01),
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177005',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1225@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        	insert ld16;
            
            test.startTest();
            	 Lead ld15 = new Lead(FirstName='tst',LastName='MyTest',Child_Due_Date__c=System.today() + 100,
                             NameOfChild__c='Jr',LastNameOfChild__c='JrTest',Country__c='Vietnam',MobilePhone='0907177002',
                             Product_Requested__c=prod.Id,Company='MyTest33Cmp',Email='test1221@test.com',
                             ActivityType__c='Purchase', Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        		insert ld15;
            
            test.stopTest();
            
        }
     }
    @isTest static void test_method_SGP() {
        List<RecordType> ldRecTypes = [SELECT Id From RecordType where Name = 'Lead SG'];
        List<RecordType> ConRecTypesPMy = [SELECT Id From RecordType where Name = 'Parent SG'];
        List<RecordType> ConRecTypesChMy = [SELECT Id From RecordType where Name = 'Child SG'];
        List<RecordType> AccntRecTypesPMy = [SELECT Id From RecordType where Name = 'Account SG'];
        Product2 prod = new Product2(Name='Friso',CurrencyIsoCode = 'SGD',isActive=true);
        insert prod;
        List<Lead> ldLst = new List<Lead>();
        Lead ld23 = new Lead(LastName='MyTest33',child_dob__c=System.today() - 40,Country__c='Singapore',ActivityType__c = 'Sample', Email='sggroup1234@hotmail.com',
                              Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        ldLst.add(ld23);
         
        Lead ld29 = new Lead(LastName='MyTest33',Country__c='Singapore', Email='sggroup1234@hotmail.com',child_dob__c=Date.newInstance(2011,01,01),
                           Product__c=prod.Id,ActivityType__c='Sample', ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,
                           LeadSource='Website',recordtypeId=ldRecTypes[0].Id);
        ldLst.add(ld29);
        Lead ld1 = new Lead(LastName='MyTest33',Country__c='Singapore',child_dob__c=Date.newInstance(2017,05,05),Email='sggroup12434@hotmail.com',
                            Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id,ActivityType__c='Sample');
        ldLst.add(ld1);
        Lead ld243 = new Lead(LastName='MyTest33',Country__c='Singapore',child_dob__c=Date.newInstance(2017,05,05),Email='sggroup12434@hotmail.com',
                            Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id,ActivityType__c='Sample');
     	ldLst.add(ld243);
        Lead ld249 = new Lead(LastName='MyTest33',Country__c='Singapore',child_dob__c=System.today() - 90,Email='sggroup12439@hotmail.com',
                            Product__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id,ActivityType__c='Newsletter');
     	ldLst.add(ld249);
        
        insert ldLst;
        
        Lead ld245 = new Lead(LastName='MyTest33',Country__c='Singapore',child_dob__c=Date.newInstance(2017,05,05),Email='sggroup12434@hotmail.com',
                            ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id,ActivityType__c='Purchase');
     	insert ld245;
        Lead ld247 = new Lead(LastName='MyTest33',Country__c='Singapore',Child_Due_Date__c=System.today() + 160,Email='sggroup12434@hotmail.com',
                            ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                            recordtypeId=ldRecTypes[0].Id,ActivityType__c='Purchase');
     	insert ld247;
        Lead ldexiPNewChild = new Lead(LastName='MyTest33',Country__c='Singapore',child_dob__c=Date.newInstance(2018,07,07),Email='sggroup12434@hotmail.com',
                            ProductPurchase__c=prod.Id,Company='MyTest33Cmp',Status ='New',IsFriso__c=true,LeadSource='Website',
                             recordtypeId=ldRecTypes[0].Id,ActivityType__c='Purchase');
     	insert ldexiPNewChild;
    }
}
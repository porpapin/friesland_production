/* Created date: 30 Sep 2019
 * Schedule to call IFFOLeadConversionBatch
 */

global class IFFOLeadConversionBatchSchedule implements Schedulable{
    
    public static String sched = '0 00 00 * * ?';  //Every Day at Midnight 

    global static String scheduleMe() {
        IFFOLeadConversionBatchSchedule SC = new IFFOLeadConversionBatchSchedule(); 
        return System.schedule('IFFOLeadConversionBatchSchedule', sched, SC);
    }

    global void execute(SchedulableContext sc) {
        System.debug('start batch');
        IFFOLeadConversionBatch b1 = new IFFOLeadConversionBatch();
        ID batchprocessid = Database.executeBatch(b1,5);           
    }
}
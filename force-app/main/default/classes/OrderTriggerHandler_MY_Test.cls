@isTest
private class OrderTriggerHandler_MY_Test {
  
    static testmethod void checkOrderCreationLessThan10Months(){
        
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        
        Id contactRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent_MY').getRecordTypeId();
        Contact con = new Contact();
        con.RecordTypeId = contactRT;
        con.LastName = 'test';
        con.AccountID = acc.id;
        con.Birthdate = system.today().addMonths(-7);
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        Id contactChildRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_MY').getRecordTypeId();
        Contact conChild = new Contact();
        conChild.RecordTypeId = contactChildRT;
        conChild.LastName = 'test';
        conChild.AccountID = acc.id;
        conChild.Birthdate = system.today().addMonths(-10);
        conChild.CurrencyIsoCode = 'USD';
        insert conChild;
        
        Contract contract = new Contract();
        contract.AccountId = acc.id;
        contract.Status ='Draft';
        contract.StartDate = system.today();
        contract.ContractTerm = 4;
        contract.CurrencyIsoCode = 'USD';
        insert contract;
        
        Id orderRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_MY').getRecordTypeId();
        Order ord = new Order();
        ord.RecordTypeId = orderRT;
        ord.AccountId = acc.id;
        ord.Contact__c = con.id;
        ord.Child_Contact__c = conChild.id;
        ord.Sample__c = prod.Id;
        ord.Type ='Sample';
        ord.ContractId = contract.id;
        ord.CurrencyIsoCode = 'USD';
        ord.EffectiveDate = system.today();
        ord.Status = 'Draft';
        try{
            insert ord;
        }catch(exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains(system.label.OrderCreationError) ? true : false;
            system.assertequals(expectedExceptionThrown,true);
        }
        
    }
    
    static testmethod void checkOrderCreationSameMonth(){
        Account acc = new Account();
        acc.Name = 'test';
        insert acc;
        
        Product2 prod = SobjectUtility.createProductMYandSG('Malaysia');
        
        Id contactRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Parent_MY').getRecordTypeId();
        Contact con = new Contact();
        con.RecordTypeId = contactRT;
        con.LastName = 'test';
        con.AccountID = acc.id;
        con.Birthdate = system.today().addMonths(-100);
        con.CurrencyIsoCode = 'USD';
        insert con;
        
        Id contactChildRT = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Child_MY').getRecordTypeId();
        Contact conChild = new Contact();
        conChild.RecordTypeId = contactChildRT;
        conChild.LastName = 'test';
        conChild.AccountID = acc.id;
        conChild.Birthdate = system.today().addMonths(-10);
        conChild.CurrencyIsoCode = 'USD';
        insert conChild;
        
        Contract contract = new Contract();
        contract.AccountId = acc.id;
        contract.Status ='Draft';
        contract.StartDate = system.today();
        contract.ContractTerm = 4;
        contract.CurrencyIsoCode = 'USD';
        insert contract;
        
        Id orderRT = Schema.SObjectType.Order.getRecordTypeInfosByDeveloperName().get('Order_MY').getRecordTypeId();
        Order ord = new Order();
        ord.RecordTypeId = orderRT;
        ord.AccountId = acc.id;
        ord.Contact__c = con.id;
        ord.Child_Contact__c = conChild.Id;
        ord.Type ='Sample';
        ord.ContractId = contract.id;
        ord.Sample__c = prod.Id;
        ord.CurrencyIsoCode = 'USD';
        ord.EffectiveDate = system.today();
        ord.Status = 'Draft';
        insert ord;
        
        Order ordNew = new Order();
        ordNew.RecordTypeId = orderRT;
        ordNew.AccountId = acc.id;
        ordNew.Contact__c = con.id;
        ordNew.Child_Contact__c = conChild.id;
        ordNew.Sample__c = prod.Id;
        ordNew.Type ='Sample';
        ordNew.ContractId = contract.id;
        ordNew.CurrencyIsoCode = 'USD';
        ordNew.EffectiveDate = system.today();
        ordNew.Status = 'Draft';
        try{
            insert ordNew;
        }catch(exception ex){
            Boolean expectedExceptionThrown =  ex.getMessage().contains(system.label.Lead_Not_Converted_Order_Exist) ? true : false;
            system.assertequals(expectedExceptionThrown,true);
        }
        
    }
}
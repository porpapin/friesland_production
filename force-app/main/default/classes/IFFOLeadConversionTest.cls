/* Created Date: 30 Sept 2019
* Test Class for IFFOLeadConversionBatch
*/ 

@isTest
public class IFFOLeadConversionTest {
    
    @testSetup 
    static void setup() {
        // Load the test data from the static resource        
        
    }
    
    
    /*static testmethod void test(){        
        
        Test.startTest();
        // setup();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            LeadStopper.setDoNotRunTrigger();
            
            String parentRecordType = RecordTypeUtility.contactParentMYRT;
            String childRecordType = RecordTypeUtility.contactChildMYRT;
            String orderRecordType = RecordTypeUtility.orderMYRT;
            String accountRecordType = RecordTypeUtility.accountMYRT;
            String leadRecordType = RecordTypeUtility.LeadMYRT;
            Date childbirth = system.today().addMonths(-10);
            
            Product2 product = new Product2(
                Name = 'Dutch Lady 123 Plain',
                IsActive=true,
                Country__c = 'Malaysia'
            );
            insert product;

            
            Lead lead1 = new Lead(
                firstname='lfirstname', 
                lastname='llastname',
                email='lfirstlast@test2.com',
                mobilephone='01100011103',
                company='test',
                leadsource='website',
                child_dob__c=system.today(),
                recordtypeid=leadRecordType,
                IsConverted=false,
                IFFO__c = false,
                Product__c = product.Id,
                
                ActivityType__c = 'Sample'
            );
            insert lead1;
            
            lead1.child_dob__c = childbirth;
            update lead1;
            
            IFFOLeadConversionBatch blc = new IFFOLeadConversionBatch();        
            Database.executeBatch(blc);
            
            
            IFFOLeadConversionBatchSchedule scheduler = new IFFOLeadConversionBatchSchedule();
            IFFOLeadConversionBatchSchedule.scheduleMe();
            // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
            String sch = '0 0 2 1/1 * ? *';
            String jobID = System.schedule('Batch Look Up Schedule Test', sch, scheduler); 
            LeadStopper.shouldRunTrigger();
        }
        Test.stopTest();   
    }*/
    
    static testmethod void test2(){        
        
        Test.startTest();
        // setup();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            LeadStopper.setDoNotRunTrigger();
            
            String parentRecordType = RecordTypeUtility.contactParentMYRT;
            String childRecordType = RecordTypeUtility.contactChildMYRT;
            String orderRecordType = RecordTypeUtility.orderMYRT;
            String accountRecordType = RecordTypeUtility.accountMYRT;
            String leadRecordType = RecordTypeUtility.LeadMYRT;
            Date childbirth = system.today().addMonths(-10);
            
            Product2 product = new Product2(
                Name = 'Friso Gold 3',
                IsActive=true,
                Country__c = 'Malaysia'
            );
            insert product;
            Account acc1 = new Account(Name='LN1',Emailaddress__c='lfirstlast@test.com',mobile__c='01100011102',recordtypeid=accountRecordType);
            insert acc1;
            
            Contact pcon = new Contact(firstname='pfirstname',lastname='lastname',recordtypeid=parentRecordType, accountId = acc1.id);
            insert  pcon;
            Contact ccon = new Contact(firstname='pfirstname',lastname='lastname',Birthdate=childbirth,recordtypeid=childRecordType, accountId = acc1.id);
            insert  ccon;   
            acc1.PrimaryContact__c = pcon.Id;
            update acc1;
            
            Lead lead1 = new Lead(
                firstname='lfirstname', 
                lastname='llastname',
                email='lfirstlast@test.com',
                mobilephone='01100011102',
                company='test',
                leadsource='website',
                child_dob__c=childbirth,
                recordtypeid=leadRecordType,
                IsConverted=false,
                Product__c = product.Id,
                Existing_Contact__c = pcon.Id,
                ActivityType__c = 'Sample'
            );
            insert lead1;
            
            
            IFFOLeadConversionBatch blc = new IFFOLeadConversionBatch();        
            Database.executeBatch(blc);
            
            
            IFFOLeadConversionBatchSchedule scheduler = new IFFOLeadConversionBatchSchedule();
            IFFOLeadConversionBatchSchedule.scheduleMe();
            // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
            String sch = '0 0 2 1/1 * ? *';
            String jobID = System.schedule('Batch Look Up Schedule Test', sch, scheduler);
            LeadStopper.shouldRunTrigger();
        }
        Test.stopTest();   
    }
    
    /*static testmethod void test3(){        
        
        Test.startTest();
        // setup();
        User thisUser = SobjectUtility.portalUserForMalaysia();
        System.runAs ( thisUser ) {
            LeadStopper.setDoNotRunTrigger();
            
            String parentRecordType = RecordTypeUtility.contactParentMYRT;
            String childRecordType = RecordTypeUtility.contactChildMYRT;
            String orderRecordType = RecordTypeUtility.orderMYRT;
            String accountRecordType = RecordTypeUtility.accountMYRT;
            String leadRecordType = RecordTypeUtility.LeadMYRT;
            Date childbirth = system.today().addMonths(-10);
            
            Product2 product = new Product2(
                Name = 'Friso Gold 3',
                IsActive=true,
                Country__c = 'Malaysia'
            );
            insert product;
            
            Lead lead1 = new Lead(
                firstname='test', 
                lastname='test',
                email='test@test.com',
                mobilephone='01100011107',
                company='test',
                leadsource='website',
                child_dob__c= childbirth,
                recordtypeid=leadRecordType,
                IsConverted=false,
                Product__c = product.Id,
                ActivityType__c = 'Sample'
            );
            insert lead1;
            
            
            
            IFFOLeadConversionBatch blc = new IFFOLeadConversionBatch();        
            Database.executeBatch(blc);
            
            
            IFFOLeadConversionBatchSchedule scheduler = new IFFOLeadConversionBatchSchedule();
            IFFOLeadConversionBatchSchedule.scheduleMe();
            // Seconds Minutes Hours Day_of_month Month Day_of_week optional_year
            String sch = '0 0 2 1/1 * ? *';
            String jobID = System.schedule('Batch Look Up Schedule Test', sch, scheduler);
            LeadStopper.shouldRunTrigger();
        }
        Test.stopTest();   
    }*/
    
    
    
    
    
}
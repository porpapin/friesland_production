trigger CreateAccountFromContact on Contact (before insert) {
    //Collect list of contacts being inserted without an account
    List<Contact> needAccounts = new List<Contact>();
    for (Contact c : trigger.new) {
        List<RecordType> rtypes = [Select DeveloperName, Id From RecordType where sObjectType='Contact' and isActive=true];
        //Create a map between the Record Type Name and Id for easy retrieval
        Map<String,String> contactRecordTypes = new Map<String,String>{};
        for(RecordType rt: rtypes){
            contactRecordTypes.put(rt.DeveloperName,rt.Id);
        }
        if (String.isBlank(c.accountid) && (c.RecordtypeId == contactRecordTypes.get('Child_MY') 
                                            || c.RecordtypeId == contactRecordTypes.get('Parent_MY')) ) {    
                                                needAccounts.add(c);
        }
    }
    
    if (needAccounts.size() > 0) {
        List<Account> newAccounts = new List<Account>();
        Map<String,Contact> contactsByNameKeys = new Map<String,Contact>();
        //Create account for each contact
        for (Contact c : needAccounts) {
            String accountName = c.firstname + ' ' + c.lastname;
            //String accountName = c.Name;
            contactsByNameKeys.put(accountName,c);
            Account a = new Account(Name=accountName);
            newAccounts.add(a);
        }
        system.debug('newAccounts--->'+newAccounts);
        insert newAccounts;
        
        List<Contact> conList = new List<Contact>();
        //Collect a new list of contacts object for new accounts
        for (Account a : newAccounts) {
            //Put account ids on contacts
            if (contactsByNameKeys.containsKey(a.Name)) {
                contactsByNameKeys.get(a.Name).accountId = a.Id;
            }
        }
    }
}
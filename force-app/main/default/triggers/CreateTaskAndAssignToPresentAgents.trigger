trigger CreateTaskAndAssignToPresentAgents on CampaignMember (after insert) 
{
    list <AgentsPresence__c> Agentlist = [select User__c from AgentsPresence__c where IsPresent__c=true];
    list<task> tasklist = new list<task>();
    integer i=0;
    string CampaignSubject='';
    List<String> cc = new List<String>();
    List<String> cc1 = new List<String>();
    Task tt = new Task();
    for(Campaignmember cm : Trigger.new)
    {
        cc.add(cm.CampaignId);
      	cc1.add(cm.ContactId);
    }
    system.debug(cc);
    system.debug(cc1);
    List<Campaign> Campaignlist = new List<Campaign>();
      if(!Agentlist.isEmpty())
        {
            if(string.isBlank(CampaignSubject))
            {
                Campaignlist = [select subject__c from campaign where id IN :cc];
                system.debug(Campaignlist);
                CampaignSubject = Campaignlist[0].subject__c;       
            }
            tt.Subject=CampaignSubject;
            tt.Priority='Normal';
            tt.Status='Open';
            for(Id cm : cc1){
                tt.WhoId = cm;
            }
            
            tt.ActivityDate = date.today();
            tt.WhatId = Campaignlist[0].Id;
            tt.OwnerId =Agentlist[i].User__c;
            i++;
            if(i==Agentlist.size())
            {
                i=0;
            }
        }
        tasklist.add(tt);
    insert tasklist;
}
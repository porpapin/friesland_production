trigger OrderTrigger on Order (before insert,after insert, before update, after update, before delete) {
    //Added Navin- 3/26/2019: I-369242
    if(trigger.isBefore && trigger.isInsert){
        OrderTriggerHandler_MY.onBeforeInsert(trigger.New);  
    }
    TriggerDispatcher.run(new OrderTriggerHandler());

}
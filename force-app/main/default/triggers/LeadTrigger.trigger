/*
 * Name : LeadTrigger
 * Created By : Sakshi Arora
 * Description: LeadTrigger on Lead object
 * Created Date: [10/31/2018]
 * Task : T-746253
 * 
 * */
trigger LeadTrigger on Lead (before insert, before update, before delete,after insert, after update, after delete, after undelete) {
        system.debug('Lead Trigger Start');
    TriggerDispatcher.run(new LeadTriggerHandler());

}
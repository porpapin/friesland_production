/*
 * Name : CaseTrigger
 * Created By : Sakshi Arora
 * Description: CaseTrigger on Case object
 * Created Date: [10/31/2018]
 * Task : T-747225
 * 
 * */
trigger CaseTrigger on Case (before insert, before update, before delete,after insert, after update, after delete, after undelete) {
    	
    TriggerDispatcher.run(new CaseTriggerHandler());

}
declare module "@salesforce/resourceUrl/CasePrintcss" {
    var CasePrintcss: string;
    export default CasePrintcss;
}
declare module "@salesforce/resourceUrl/DutchLadyLogo" {
    var DutchLadyLogo: string;
    export default DutchLadyLogo;
}
declare module "@salesforce/resourceUrl/FCLogo" {
    var FCLogo: string;
    export default FCLogo;
}
